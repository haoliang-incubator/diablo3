﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "barbarian attack whirlwind"
;}}}

#Include %A_ScriptDir%\lib\support_common.ahk

; labels
; {{{

WrathOfTheBerserker:
SendInput {space down}{click left}{space up}
return

BattleRage:
SendInput {click right}
return

Sprint:
SendInput s
return

;}}}

e::
eToggle := ! eToggle
if eToggle {
	SetTimer, WrathOfTheBerserker, % 1000 * 3
	SetTimer, BattleRage, % 1000 * 60 * 1.5
	SetTimer, FuriousCharge, 500

	Gosub, WrathOfTheBerserker
	Gosub, BattleRage
} else {
	SetTimer, WrathOfTheBerserker, off
	SetTimer, BattleRage, off
	SetTimer, FuriousCharge, off
}
return

$f::
ToggleOff(["q"])
SendInput, {f down}
SetTimer, Sprint, 2950
Gosub, Sprint
KeyWait, f
SetTimer, Sprint, off
SendInput, {f up}
ToggleOn(["q"])
return


; 回城
z::
ToggleOff(["q", "e"])
sleep, 75
SendInput, {h}
return



#IfWinActive
