﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "barbarian attack - 90"

APP_STARTUP_LABELS := ["q", "e"]
;}}}

; includes
;{{{
#Include %A_ScriptDir%\lib\support_common.ahk
;}}}

; labels
; {{{

ThreateningShout:
SendInput f
return

BattleRage:
SendInput {space down}{click right}{space up}
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
if eToggle {

	SetTimer, ThreateningShout, 1500
    Gosub, ThreateningShout

	SetTimer, WarCry, 4000
    Gosub, WarCry

	SetTimer, FuriousCharge, 500

	SetTimer, BattleRage, 10000
    Gosub, BattleRage

} else {
	SetTimer, BattleRage, off
	SetTimer, ThreateningShout, off
	SetTimer, FuriousCharge, off
	SetTimer, WarCry, off
}
return
;}}}

#IfWinActive
