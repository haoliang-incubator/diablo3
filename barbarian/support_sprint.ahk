﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "barbarian support - sprint"

APP_STARTUP_LABELS := ["e"]
;}}}

; includes
;{{{
#Include %A_ScriptDir%\lib\support_common.ahk
;}}}

; labels
; {{{

Sprint:
SendInput s
return

ThreateningShout:
SendInput, {space down}{click left}{space up}
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
if eToggle {
	SetTimer, Sprint, 2750
	SetTimer, ThreateningShout, 1500
	SetTimer, WarCry, 4000
	SetTimer, FuriousCharge, 500

    Gosub, Sprint
    Gosub, WarCry
} else {
	SetTimer, Sprint, off
	SetTimer, ThreateningShout, off
	SetTimer, FuriousCharge, off
	SetTimer, WarCry, off
}
return
;}}}


#IfWinActive
