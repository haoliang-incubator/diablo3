﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions ;{{{
APP_NAME := "barbarian support - stomp"
APP_STARTUP_LABELS := ["e"]
;}}}

; includes ;{{{
#Include %A_ScriptDir%\lib\support_common.ahk
;}}}

; labels ;{{{
ThreateningShout:
SendInput, {space down}{click left}{space up}
return

;}}}

; hotkeys ;{{{

$s::
; 践踏也会触发力量戒指减伤效果
; _LAST_CHARGE_ comes from lib/furious_charge.ahk
global _LAST_CHARGE_ := A_TickCount
; 践踏前释放 威吓呐喊
SendInput, {space down}{click left}{space up}{s}
return

e::
eToggle := !eToggle
if eToggle {
	SetTimer, ThreateningShout, % 1000 * 12
	SetTimer, FuriousCharge, 1000
	SetTimer, WarCry, % 1000 * 10

    Gosub, FuriousCharge
	Gosub, WarCry
} else {
	SetTimer, ThreateningShout, off
	SetTimer, FuriousCharge, off
	SetTimer, WarCry, off
}
return

; 回城
z::
ToggleOff(["q", "e"])
Gosub, IgnorePain
Gosub, $s
sleep, 600 ; stomp has 500ms channel time
SendInput, {h}
return

;}}}

#IfWinActive
