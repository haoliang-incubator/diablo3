﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "barbarian support - sprint"

APP_STARTUP_LABELS := ["q", "e", "t"]
;}}}

; includes
;{{{
#Include %A_ScriptDir%\lib\support_common.ahk
;}}}

; labels
; {{{

Sprint:
SendInput {click right}
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
if eToggle {
	SetTimer, Sprint, 2750
	SetTimer, WarCry, 4000
	SetTimer, FuriousCharge, 500

    Gosub, Sprint
    Gosub, WarCry
} else {
	SetTimer, Sprint, off
	SetTimer, FuriousCharge, off
	SetTimer, WarCry, off
}
return
;}}}

t:: ;{{{
tToggle := !tToggle
if tToggle {
    SetTimer, IgnorePain, 4750
} else {
    SetTimer, IgnorePain, off
}
return
;}}}

; 回城
z::
ToggleOff(["q", "e", "t"])
startupToggle := false
Gosub, IgnorePain
sleep, % 2 * Ceil(1000/60)
SendInput, {h}
return


#IfWinActive
