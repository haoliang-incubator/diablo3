﻿; requires lib/function/cursor

; definitions
;{{{
_LAST_CHARGE_ := 0
;}}}

_furiouscharge() {
    global _LAST_CHARGE_

    uninit := _LAST_CHARGE_ <= 0
    cooldown :=  A_TickCount - _LAST_CHARGE_ > 7500


    if not (uninit or cooldown)
        return

    MouseGetPos, curx, cury

    tox := A_ScreenWidth/2
    toy := A_ScreenHeight/2 + 10
    MouseMove, %tox%, %toy%, 0
    Gosub, $a

    MouseMove, %curx%, %cury%, 0
}


; labels
;{{{
; FuriousCharge will last 8s
FuriousCharge:
_furiouscharge()
return
;}}}

; see label **FuriousCharge**
$a:: ;{{{
global _LAST_CHARGE_ := A_TickCount
SendInput, {a}
return
;}}}
