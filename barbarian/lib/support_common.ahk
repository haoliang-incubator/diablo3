﻿
; author: haoliang<haoliang0.1.2@gmail.com>

; includes ;{{{
#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\toggle.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
#Include %A_ScriptDir%\..\lib\keybind\startup.ahk
#Include %A_ScriptDir%\..\lib\keybind\access.ahk

#Include %A_ScriptDir%\lib\furious_charge.ahk
;}}}

; labels ;{{{
IgnorePain:
SendInput f
return

WarCry:
SendInput d
return
;}}}
