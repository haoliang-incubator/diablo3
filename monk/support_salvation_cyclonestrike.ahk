﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "monk support - salvation"

; q 左击
; w 强制站立
; e 隐式技能
; t 眩目闪
; y 金轮阵
; u 飓风破
; i 飓风破+断筋诀前2击

APP_ALL_LABELS := ["q", "w", "e", "t", "y", "u", "i", "o"]
APP_STARTUP_LABELS := ["q", "e", "t", "y", "o"]
APP_MARCH_LABELS := ["q", "e", "y", "u"]
APP_BOSS_LABELS := ["i", "e", "y"]

PeriodManatraOfSalvation := 2000

; todo
; race condition: CycloneStrike, BlindingFlash, InnerSanctuary
PeriodEpiphany := 750
PeriodCycloneStrike := 4500
PeriodBlindingFlash := 750
PeriodForceStand := 500
PeriodInnerSanctuary := 1250

;}}}

; includes
;{{{
#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\cursor.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\toggle.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk
#Include %A_ScriptDir%\..\lib\function\salvage.ahk

#Include %A_ScriptDir%\..\lib\keybind\access.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\scavenger.ahk
#Include %A_ScriptDir%\..\lib\keybind\management.ahk

#Include %A_ScriptDir%\lib\cyclone_strike.ahk
#Include %A_ScriptDir%\lib\scene.ahk

;}}}

; labels
; {{{

ClickLeft:
Click left
return

CripplingWave:
Click left
return

ManatraOfSalvation:
Click right
return

OriginCycloneStrike:
SendInput s
return

ForceCycloneStrike:
SetTimer, OriginCycloneStrike, 350
Sleep 700
SetTimer, OriginCycloneStrike, off
return

Epiphany:
SendInput a
return

BlindingFlash:
SendInput d
return

InnerSanctuary:
SendInput f
return

ForceStand:
Send {Space down}
return

RelieveForceStand:
Send {Space up}
return

CripplingWaveAndCycloneStrike:
Gosub, CripplingWave
Sleep 250
Gosub, CripplingWave
Sleep 250
Gosub, OriginCycloneStrike
Sleep 100
return

; }}}

; 隐式技能
; * 灵光悟 1/s (epiphany)
; * 救赎真言 1/2750ms (manatra of salvation)
e:: ;{{{
eToggle := !eToggle
if eToggle {
	SetTimer, Epiphany, %PeriodEpiphany%
	Gosub, Epiphany

	SetTimer, ManatraOfSalvation, %PeriodManatraOfSalvation%
	Gosub, ManatraOfSalvation
} else {
	SetTimer, Epiphany, off
	SetTimer, ManatraOfSalvation, off
}
return ;}}}

; 站立
w:: ;{{{
wToggle := !wToggle
if wToggle {
    SetTimer, ForceStand, %PeriodForceStand%
} else {
    SetTimer, ForceStand, off
    Goto, RelieveForceStand
}
return
;}}}

; 金轮阵 (inner sanctuary)
y:: ;{{{
yToggle := !yToggle
If yToggle {
	SetTimer, InnerSanctuary, %PeriodInnerSanctuary%
	Gosub, InnerSanctuary
} Else {
	SetTimer, InnerSanctuary, off
}
return
;}}}

; 眩目闪 (控制递减，boss时需要关闭; 所以单独控制)
t:: ;{{{
tToggle := !tToggle
if tToggle {
    SetTimer, BlindingFlash, %PeriodBlindingFlash%
    Gosub, BlindingFlash
} else {
    SetTimer, BlindingFlash, off
}
return ;}}}

; 断筋诀+飓风破 combo
; 不打出 断筋诀的第三击
i:: ;{{{
iToggle := !iToggle
if iToggle {
    SetTimer, CripplingWaveAndCycloneStrike, 750
} else {
    SetTimer, CripplingWaveAndCycloneStrike, off
}
return ;}}}

; bug, label `w` 会影响鼠标左击
LButton:: ;{{{
if wToggle {
    Gosub, w
} else {
    SendInput {Space up}
}
Click left
return ;}}}

; 飓风破
u:: ;{{{
uToggle := !uToggle
if uToggle {
    SetTimer, CycloneStrike, 500
} else {
    SetTimer, CycloneStrike, off
}
return ;}}}

; 飓风破
o:: ;{{{
oToggle := !oToggle
if oToggle {
    SetTimer, OriginCycloneStrike, 500
} else {
    SetTimer, OriginCycloneStrike, off
}
return ;}}}

q:: ;{{{
qToggle := !qToggle
if qToggle {
    SetTimer, ClickLeft, 250
} else {
    SetTimer, ClickLeft, off
}
return ;}}}

#IfWinActive
