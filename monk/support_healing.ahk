﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "monk support - healing"

APP_STARTUP_LABELS := ["q", "e", "t", "y", "u"]

; transmission
MY_TRANSMISSION_COUNT := 0
MY_TRANSMISSION_MODS := [125, 250]

; timer flag
TimerManatraHealingRuning := false

; see MY_TRANSMISSION_COUNT[1]
PeriodManatraHealing := 250
;}}}

; includes
;{{{
#Include %A_ScriptDir%\..\lib\function\transmission.ahk
#Include %A_ScriptDir%\lib\support_common.ahk
;}}}

; labels
; {{{

ManatraHealing:
Click right
return

;}}}

; funcs
;{{{
ManatraHealingTransmissionSpeed() {
    global MY_TRANSMISSION_COUNT
    global MY_TRANSMISSION_MODS

    return TransmissionSpeed(MY_TRANSMISSION_COUNT, MY_TRANSMISSION_MODS)
}
;}}}

; 隐式技能
; * 灵光悟 1/s (epiphany)
; * 治愈真言 1/2750ms (manatra of healing)
e:: ;{{{
eToggle := !eToggle
if eToggle {
	SetTimer, Epiphany, %PeriodEpiphany%
    Gosub, Epiphany

	SetTimer, ManatraHealing, %PeriodManatraHealing%
    Gosub, ManatraHealing
    TimerManatraHealingRuning := true
} else {
	SetTimer, Epiphany, off

	SetTimer, ManatraHealing, off
    TimerManatraHealingRuning := false
}
return ;}}}

; 治愈真言 变速器
v:: ;{{{
title := "ManatraHealing transmission"
if TimerManatraHealingRuning {
    ; keep increasement first to avoid race condition
    MY_TRANSMISSION_COUNT ++
    speed := ManatraHealingTransmissionSpeed()
    SetTimer, ManatraHealing, off
    SetTimer, ManatraHealing, %speed%
    Notify(title, "current speed: " . speed)
} else {
    Notify(title, "CANNOT change speed, as it wasnt running")
}
return ;}}}

; 查看当前状态
g:: ;{{{
title := "reflection"

info := []
info.Push(Alternative(qToggle, "[o]", "[x]") . " click left")
info.Push(Alternative(eToggle, "[o]", "[x]") . " implicit skills")
info.Push(Alternative(wToggle, "[o]", "[x]") . " stand still")
info.Push(Alternative(yToggle, "[o]", "[x]") . " inner sanctuary")
info.Push(Alternative(tToggle, "[o]", "[x]") . " blinding flash")
info.Push("`n")
info.Push("manatra healing speed: " . ManatraHealingTransmissionSpeed())

msg := Join(info, " ", " ")

Notify(title, msg, 1000)
return
;}}}

#IfWinActive
