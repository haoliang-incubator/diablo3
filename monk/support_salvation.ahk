﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "monk support - salvation"

APP_STARTUP_LABELS := ["q", "e", "t", "y", "u"]

PeriodManatraOfSalvation := 2550
;}}}

; includes
;{{{
#Include %A_ScriptDir%\lib\support_common.ahk
;}}}

; labels
; {{{

ManatraOfSalvation:
Click right
return

; }}}

; 隐式技能
; * 灵光悟 1/s (epiphany)
; * 救赎真言 1/2750ms (manatra of salvation)
e:: ;{{{
eToggle := !eToggle
if eToggle {
	SetTimer, Epiphany, %PeriodEpiphany%
	Gosub, Epiphany

	SetTimer, ManatraOfSalvation, %PeriodManatraOfSalvation%
	Gosub, ManatraOfSalvation
} else {
	SetTimer, Epiphany, off
	SetTimer, ManatraOfSalvation, off
}
return ;}}}

#IfWinActive
