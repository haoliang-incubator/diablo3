﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; epiphany: 灵光悟
; sweeping wind: 劲风煞
; tempest rush: 风雷冲
; serenity: 禅定
; cyclone strike: 飓风破
; dashing strike: 疾风击

APP_NAME := "monk - attack justice set"

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk

#Include %A_ScriptDir%\..\lib\keybind\access.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
#Include %A_ScriptDir%\..\lib\keybind\management.ahk


Serenity:
SendInput a
return

SweepingWind:
SendInput d
return

Epiphany:
Click right
return


e::
eToggle := !eToggle
if eToggle {
    SetTimer, Epiphany, 14500
    Gosub, Epiphany
    SetTimer, SweepingWind, 5500
    Gosub, SweepingWind
    SetTimer, Serenity, 250
} else {
    SetTimer, Epiphany, off
    SetTimer, SweepingWind, off
    SetTimer, Serenity, off
}
return

f::
if WheelUpToggle {
    Gosub, WheelUp
}
SendInput {f down}
return

f up::
SendInput {f up}
Gosub, WheelUp
return


#IfWinActive
