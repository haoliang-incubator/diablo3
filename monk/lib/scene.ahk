﻿; todo
; * notification
; * mutual exclusive
; * dynamic generate

; requires **APP_ALL_LABELS**

; 站桩 startup
F5:: ;{{{

startupToggle := !startupToggle

if (isArrayEmpty(APP_STARTUP_LABELS)) {
    Notify("startup", "lack of APP_STARTUP_LABELS definition.")
    return
}

if startupToggle {
    ToggleOff(APP_ALL_LABELS)
    marchToggle := false
    bossToggle := false
}

; first run, startupToggle == true
if startupToggle {
    Notify("startup", "initializing")
    ToggleOn(APP_STARTUP_LABELS)
} else {
    Notify("startup", "relieving")
    ToggleOff(APP_STARTUP_LABELS)
}
return ;}}}

; 赶路 march
F6:: ;{{{

marchToggle := !marchToggle

if (isArrayEmpty(APP_MARCH_LABELS)) {
    Notify("march", "lack of APP_MARCH_LABELS definition.")
    return
}

if marchToggle {
    ToggleOff(APP_ALL_LABELS)
    startupToggle := false
    bossToggle := false
}

; first run, marchToggle == true
if marchToggle {
    Notify("march", "initializing")
    ToggleOn(APP_MARCH_LABELS)
} else {
    Notify("march", "relieving")
    ToggleOff(APP_MARCH_LABELS)
}
return ;}}}

; 杀王 boss
F7:: ;{{{
bossToggle := !bossToggle

if (isArrayEmpty(APP_BOSS_LABELS)) {
    Notify("boss", "lack of APP_BOSS_LABELS definition.")
    return
}

if bossToggle {
    ToggleOff(APP_ALL_LABELS)
    startupToggle := false
    marchToggle := false
}

; first run, bossToggle == true
if bossToggle {
    Notify("boss", "initializing")
    ToggleOn(APP_BOSS_LABELS)
} else {
    Notify("boss", "relieving")
    ToggleOff(APP_BOSS_LABELS)
}
return ;}}}
