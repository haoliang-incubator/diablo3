﻿
MY_S_LAST_PRESS := 0

CycloneStrike:
if (MY_S_LAST_PRESS <= 0) {
    Goto, $s
}
if (A_TickCount - MY_S_LAST_PRESS > 4500) {
    Goto, $s
}
return

$s::
MY_S_LAST_PRESS := A_TickCount
SendInput s
return
