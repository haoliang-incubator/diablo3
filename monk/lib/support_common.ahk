﻿
; author: haoliang<haoliang0.1.2@gmail.com>

; definitions
;{{{

; todo
; race condition: CycloneStrike, BlindingFlash, InnerSanctuary
PeriodEpiphany := 750
PeriodCycloneStrike := 4500
PeriodBlindingFlash := 5750
PeriodForceStand := 500
PeriodInnerSanctuary := 1250

;}}}

; includes
;{{{

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\cursor.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\toggle.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk
#Include %A_ScriptDir%\..\lib\function\salvage.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
#Include %A_ScriptDir%\..\lib\keybind\scavenger.ahk
#Include %A_ScriptDir%\..\lib\keybind\startup.ahk
#Include %A_ScriptDir%\..\lib\keybind\access.ahk

;}}}

; labels
; {{{

CycloneStrike:
SendInput s
return

ForceCycloneStrike:
SetTimer, CycloneStrike, 350
Sleep 700
SetTimer, CycloneStrike, off
return

Epiphany:
SendInput a
return

BlindingFlash:
SendInput d
return

InnerSanctuary:
SendInput f
return

ForceStand:
Send {Space down}
return

RelieveForceStand:
Send {Space up}
return

;}}}

; 眩目闪 (控制递减，boss时需要关闭; 所以单独控制)
t:: ;{{{/*{{{*/
tToggle := !tToggle
if tToggle {
    SetTimer, BlindingFlash, %PeriodBlindingFlash%
    Gosub, BlindingFlash
} else {
    SetTimer, BlindingFlash, off
}
return ;}}}/*}}}*/

; 站立
w:: ;{{{
wToggle := !wToggle
If wToggle
    SetTimer, ForceStand, %PeriodForceStand%
Else
    SetTimer, ForceStand, off
    Goto, RelieveForceStand
return
;}}}

; 金轮阵 (inner sanctuary)
y:: ;{{{
yToggle := !yToggle
If yToggle {
	SetTimer, InnerSanctuary, %PeriodInnerSanctuary%
	Gosub, InnerSanctuary
} Else {
	SetTimer, InnerSanctuary, off
}
return
;}}}

; 飓风破 (cyclone strike)
u:: ;{{{
uToggle := !uToggle
if uToggle {
	SetTimer, ForceCycloneStrike, %PeriodCycloneStrike%
	Gosub, ForceCycloneStrike
} else {
	SetTimer, ForceCycloneStrike, off
}
return ;}}}

; off when boss, rush
F6:: ;{{{
labels := ["t"]

ToggleOff(labels)
return ;}}}
