﻿
; author: haoliang<haoliang0.1.2@gmail.com>

; * 烈焰斩 (slash)
; * 钢铁之肤 30/7s (iron skin)
; * 挑衅 20/4s (provoke)
; * 勇气律法 30/5s (laws of valor)
; * 天谴 15/3s (condemn)
; * 阿卡拉特勇士 90/20s (akarat's champion)

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "crusader attack"

MY_TRANSMISSION_COUNT := 0
MY_TRANSMISSION_MODS := [750, 250]

MY_TRANSMISSION_CURRENT_SPEED := 750

MY_TIMER_CONDEMN_RUNING := false

APP_STARTUP_LABELS := ["e", "q"]

;}}}

; includes {{{
#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\transmission.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
;}}}

; labels
; {{{

Provoke:
SendInput a
return

IronSkin:
SendInput s
return

AkaratChampion:
SendInput f
return

LawsOfValor:
SendInput d
return

Condemn:
Click right
return

SteedCharge:
SendInput {space down}{click left}{space up}
return

;}}}

; func
;{{{
CondemnTransmissionSpeed() {
    global MY_TRANSMISSION_COUNT
    global MY_TRANSMISSION_MODS

    return TransmissionSpeed(MY_TRANSMISSION_COUNT, MY_TRANSMISSION_MODS)
}
;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, IronSkin, 750
	SetTimer, AkaratChampion, 750
	SetTimer, LawsOfValor, 750
	SetTimer, Provoke, 750
    SetTimer, Condemn, %MY_TIMER_CONDEMN_RUNING%
    MY_TIMER_CONDEMN_RUNING := true
} Else {
	SetTimer, IronSkin, off
	SetTimer, AkaratChampion, off
	SetTimer, LawsOfValor, off
	SetTimer, Provoke, off
    SetTimer, Condemn, off
    MY_TIMER_CONDEMN_RUNING := false
}
return
;}}}

; 天谴，换档
v:: ;{{{
if MY_TIMER_CONDEMN_RUNING {
    MY_TRANSMISSION_COUNT ++
    MY_TRANSMISSION_CURRENT_SPEED := CondemnTransmissionSpeed()
    SetTimer, Condemn, off
    SetTimer, Condemn, %MY_TRANSMISSION_CURRENT_SPEED%
    Notify("condemn transmission", "current speed: " . MY_TRANSMISSION_CURRENT_SPEED)
} else {
    Notify("condemn transmission", "canot change condemn speed as it isnt enabled.")
}
return ;}}}

; display status
g:: ;{{{

info := []
info.Push("condemn speed: " . MY_TRANSMISSION_CURRENT_SPEED)
msg := Join(info, "`n", "- ")

Notify("reflection", msg)
return ;}}}

; steed charge
RButton:: ;{{{
;TypeInUnicode("嘚~~驾~~")
Pause, on, 1

; last 3s
Gosub, SteedCharge
; force move
Gosub, ForceMove

; off
Sleep 3000
Gosub, RelieveForceMove

Pause, off
;TypeInUnicode("yu~~")
return ;}}}

#IfWinActive
