﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "barbarian attack - hammer"
;}}}

; includes
;{{{
#Include %A_ScriptDir%\..\lib\meta\set.ahk
#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\access.ahk
#Include %A_ScriptDir%\lib\furious_charge.ahk
;}}}

; labels
; {{{

ThreateningShout:
SendInput s
return

IgnorePain:
SendInput d
return

BattleRage:
SendInput {Space down}
Click left
SendInput {Space up}
return

WrathOfTheBerserker:
SendInput f
return

;}}}


; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, BattleRage, 10000
    Gosub, BattleRage
    SetTimer, IgnorePain, 750
    SetTimer, ThreateningShout, 750
    SetTimer, FuriousCharge, 500
    ;SetTimer, WrathOfTheBerserker, 3750
} Else {
	SetTimer, BattleRage, off
    SetTimer, IgnorePain, off
    SetTimer, ThreateningShout, off
    SetTimer, FuriousCharge, off
    ;SetTimer, WrathOfTheBerserker, off
}
return
;}}}

#IfWinActive
