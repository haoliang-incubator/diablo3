﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "barbarian attack"
;}}}

#Include %A_ScriptDir%\lib\function\notification.ahk
#Include %A_ScriptDir%\lib\keybind\management.ahk
#Include %A_ScriptDir%\lib\keybind\jargon.ahk

; labels
; {{{

Sprint:
Click right
return

ThreateningShout:
SendInput s
return

IgnorePain:
SendInput d
return

BattleRage:
SendInput f
return

ClickDown:
Click, down
return

RelieveClickDown:
Click, up
return

ForceStand:
Send {Space down}
return

RelieveForceStand:
Send {Space up}
return

FuriousCharge:
SendInput a
return

ForceFuriousCharge:
SetTimer, FuriousCharge, 200
Sleep 1000
SetTimer, FuriousCharge, off
return

WarCry:
SendInput d
return

;}}}

q:: ;{{{
qToggle := !qToggle
if qToggle {
    SetTimer, ClickDown, 250
} else {
    SetTimer, ClickDown, off
    Goto, RelieveClickDown
}
return ;}}}

; 站立
w:: ;{{{
wToggle := !wToggle
if wToggle {
    SetTimer, ForceStand, 250
} else {
    SetTimer, ForceStand, off
    Goto, RelieveForceStand
}
return
;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, BattleRage, 10000
	SetTimer, Sprint, 3000
	SetTimer, ThreateningShout, 1250
	SetTimer, WarCry, 1250
    SetTimer, ForceFuriousCharge, 7000
	;SetTimer, IgnorePain, 750
} Else {
	SetTimer, BattleRage, off
	SetTimer, Sprint, off
	SetTimer, ThreateningShout, off
	SetTimer, WarCry, off
    SetTimer, ForceFuriousCharge, off
	;SetTimer, IgnorePain, off
}
return
;}}}

#IfWinActive
