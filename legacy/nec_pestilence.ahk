﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 死寒之地 1/s (land of dead)
; 吞噬 1/100ms (devour)
; 血魂双分 1/750ms 120s (simulacrum)
; 鲜血奔行 5s (blood rush)
; 尸矛 1/750ms (corpse lance)
; 诅咒 (curses - decrepify)

;#MaxThreadsPerHotKey 1

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "nec pestilence"
;}}}

#Include %A_ScriptDir%\lib\function\notification.ahk
#Include %A_ScriptDir%\lib\keybind\management.ahk
#Include %A_ScriptDir%\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\lib\keybind\click.ahk

; labels
;{{{

CropseLance:
SendInput d
return

LandOfDead:
SendInput a
return

Devour:
SendInput f
return

;}}}

; 一些隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Devour, 100
	SetTimer, CropseLance, 100
	;SetTimer, LandOfDead, 1250
} Else {
	SetTimer, Devour, off
	SetTimer, CropseLance, off
	;SetTimer, LandOfDead, off
}
return ;}}}

#IfWinActive
