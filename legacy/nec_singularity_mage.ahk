﻿
#IfWinActive ahk_class D3 Main Window Class


; toggle 脚本
;{{{
^c::
Suspend
return
t::
Pause
return
;}}}

; 一些隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Devour, 100
	SetTimer, CommandSkeletons, 100
} Else {
	SetTimer, CommandSkeletons, off
	SetTimer, Devour, off
}
return
;}}}

; labels
;{{{

Devour:
SendInput f
return

CommandSkeletons:
SendInput d
return

;}}}

#IfWinActive
