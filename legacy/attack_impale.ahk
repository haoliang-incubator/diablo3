; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "dh attack - 3"

; vengeance 复仇 750ms
; shadow power 暗影之力 4950ms
; companion 战宠 750ms
; preparation 2500ms

;}}}

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk

; labels
; {{{

Vengeance:
SendInput d
return

ShadowPower:
SendInput f
return

Preparation:
SendInput {Space down}
Click left
SendInput {Space up}
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Vengeance, 750

	; SetTimer, Companion, 750
	SetTimer, Preparation, 2750

	SetTimer, ShadowPower, 6950
	Gosub, ShadowPower
} Else {
	SetTimer, Vengeance, off
	;SetTimer, Companion, off
	SetTimer, Preparation, off
	SetTimer, ShadowPower, off
}
return
;}}}

#IfWinActive
