﻿
; author: haoliang<haoliang0.1.2@gmail.com>
; see http://bbs.d.163.com/forum.php?mod=viewthread&tid=173531275

; 魔法飞弹 (magic missile)
; 陨石术 (meteor)
; 奥术洪流 (arcane torrent)
; 瓦解射线 (disintegrate)

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "wizard firebird"
;}}}

; includes
;{{{
#Include %A_ScriptDir%\lib\function\notification.ahk
#Include %A_ScriptDir%\lib\function\typein.ahk

#Include %A_ScriptDir%\lib\keybind\management.ahk
#Include %A_ScriptDir%\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\lib\keybind\click.ahk
;}}}

;labels
;{{{

Meteor:
SendInput a
return

MagicMissile:
Click left
return

; includes:
; * arcane torrent
; * disintegrate
ChannelSkill:
Click, down, right
return
RelieveChannelSkill:
Click, up, right
return

ForceStand:
SendInput {Space down}
return
RelieveForceStand:
SendInput {Space up}
return

;}}}

; life cycle
;
; meteor -------------------------------------->
;          magic missile --->
;                            arcane torrent --->
;
t:: ;{{{

SetTimer, ForceStand, 250

; meteor
SendInput a
Sleep 275

; magic missile, for 10 mana
SetTimer, MagicMissile, 150
Sleep 450
SetTimer, MagicMissile, off

; channeling
SetTimer, ChannelSkill, 100
Sleep 850
SetTimer, ChannelSkill, off
Gosub, RelieveChannelSkill

SetTimer, ForceStand, off
Gosub, RelieveForceStand

return ;}}}

#IfWinActive

