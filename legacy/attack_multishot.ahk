﻿; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "dh attack - 3"

; vengeance ���� 750ms
; shadow power ��Ӱ֮�� 4950ms
; companion ս�� 750ms
; preparation 2500ms

;}}}

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk

; labels
; {{{

Vengeance:
SendInput d
return

ShadowPower:
SendInput f
return

Preparation:
SendInput {Space down}
Click left
SendInput {Space up}
return

;}}}

e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Vengeance, 19900
	Gosub, Vengeance

	SetTimer, Preparation, 2750

	SetTimer, ShadowPower, 6950
	Gosub, ShadowPower
} Else {
	SetTimer, Vengeance, off
	SetTimer, Preparation, off
	SetTimer, ShadowPower, off
}
return
;}}}

#IfWinActive
