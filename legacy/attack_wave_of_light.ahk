﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "monk attack - wave of light"
;}}}

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk

; labels
; {{{

Epiphany:
SendInput a
return

Manatra:
SendInput f
return

SweepingWind:
SendInput d
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Manatra, 2750
    Gosub, Manatra

	SetTimer, Epiphany, 750

	SetTimer, SweepingWind, 10000
    Gosub, SweepingWind
} Else {
	SetTimer, Manatra, off
	SetTimer, Epiphany, off
	SetTimer, SweepingWind, off
}
return
;}}}

#IfWinActive
