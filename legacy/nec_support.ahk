﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 死寒之地 1/s (land of dead)
; 吞噬 1/250ms (devour)
; 瘟疫新星 1/250ms (death nova)
; 尸矛 1/750ms (cropse lance)

#SingleInstance force

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "nec support"
;}}}

#Include %A_ScriptDir%\lib\meta\set.ahk
#Include %A_ScriptDir%\lib\function\notification.ahk
#Include %A_ScriptDir%\lib\function\cursor.ahk
#Include %A_ScriptDir%\lib\function\typein.ahk

#Include %A_ScriptDir%\lib\keybind\management.ahk
#Include %A_ScriptDir%\lib\keybind\jargon.ahk
#Include %A_ScriptDir%\lib\keybind\menu.ahk

; labels
;{{{

LandOfDead:
SendInput a
return

CropseLance:
Sendinput d
return

BoneSpear:
Click left
return

Devour:
SendInput f
return

Frailty:
Click right
return

ForceFrailty:
SetTimer, Frailty, 250
Sleep, 1000
SetTimer, Frailty, off
return

;}}}

; 黄道刷cd
q:: ;{{{
qToggle := !qToggle
if qtoggle {
	SetTimer, BoneSpear, 250
} else {
	SetTimer, BoneSpear, off
}
return ;}}}

; 死寒之地
y:: ;{{{
yToggle := !yToggle
if wToggle {
	SetTimer, LandOfDead, 250
} else {
	SetTimer, LandOfDead, off
}
return
;}}}

; 一些隐式技能
e:: ;{{{
eToggle := !eToggle
if etoggle {
	SetTimer, Devour, 200
	SetTimer, CropseLance, 200
    SetTimer, ForceFrailty, 5000
} else {
	SetTimer, Devour, off
	SetTimer, CropseLance, off
	SetTimer, ForceFrailty, off
}
return ;}}}

#IfWinActive
