﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "witch doctor attack"
;}}}

#Include %A_ScriptDir%\lib\function\notification.ahk
#Include %A_ScriptDir%\lib\keybind\management.ahk
#Include %A_ScriptDir%\lib\keybind\jargon.ahk

; labels
;{{{

Horrify:
SendInput f
return

Haunt:
Click left
return

LocustSwarm:
Click right
return

ForceMove:
SendInput {r down}
return

RelieveForceMove:
SendInput {r up}
return

ForceStand:
SendInput {Space down}
return
RelieveForceStand:
SendInput {Space up}
return

;}}}

; force move
q:: ;{{{
qToggle := !qToggle
if qToggle {
    SetTimer, ForceMove, 500
} else {
    SetTimer, ForceMove, off
    Gosub, RelieveForceMove
}
return ;}}}

; implicit skill
e:: ;{{{
eToggle := !eToggle
if eToggle {
    SetTimer, Horrify, 2000
} else {
    SetTimer, Horrify, off
}
return ;}}}

w:: ;{{{
wToggle := !wToggle
if wToggle {
    SetTimer, ForceStand, 500
    SetTimer, Haunt, 250
    SetTimer, LocustSwarm, 500
} else {
    SetTimer, ForceStand, off
    Gosub, RelieveForceStand
    SetTimer, Haunt, off
    SetTimer, LocustSwarm, off
}
return ;}}}

#IfWinActive
