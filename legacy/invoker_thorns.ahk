﻿
; author: haoliang<haoliang0.1.2@gmail.com>

; * 烈焰斩 (slash)
; * 钢铁之肤 30/7s (iron skin)
; * 挑衅 20/4s (provoke)
; * 勇气律法 30/5s (laws of valor)
; * 阿卡拉特勇士 90/20s (akarat's champion)

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "crusader 荆棘"
;}}}

; includes {{{
#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\typein.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\jargon.ahk
;}}}

; labels
; {{{

Provoke:
Click right
return

IronSkin:
SendInput a
return

AkaratChampion:
SendInput f
return

LawsOfValor:
SendInput d
return

Attack:
Click left
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Attack, 150
	SetTimer, AkaratChampion, 750
	SetTimer, LawsOfValor, 1250
	SetTimer, Provoke, 750
} Else {
	SetTimer, Attack, off
	SetTimer, AkaratChampion, off
	SetTimer, LawsOfValor, off
	SetTimer, Provoke, off
}
return
;}}}

;
; combo: S + A
;

$s:: ;{{{
Pause, On, 1
SendInput {s}
return ;}}}


$a:: ;{{{
Pause, Off
SendInput {a}
return ;}}}

#IfWinActive
