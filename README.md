﻿# diablo3_assist
scripts for diablo3, free hands

## how to use

1. install the [AutoHotKey](https://github.com/Lexikos/AutoHotkey_L)
2. change key mappings in script
2. run the script by autohotkey or compile it


## coding guides

### const naming

snake: upper letter + under score

### variable name

* global variable: Capitalize the first letter + camel style
* local variable: underscore + lower first letter + camel style

### file encoding

`utf8-bomb`

set in vim: `set fileencoding=utf8 | set bomb`

why: https://autohotkey.com/board/topic/91711-unicode-bom/


## onlie resource

* https://www.diablofans.com/builds/98593-support-monk-an-in-depth-guide-to-zmonk


## debug tips

* `fileencoding` = utf8, bomb
* `return` keyword