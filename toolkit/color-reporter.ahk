﻿
#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk

curx := 0
cury := 0

F5::
MouseGetPos, curx, cury
PixelGetColor, colour, curx, cury
Notify("color picker", "coor " . curx . "," . cury . "; color: " . colour)
return

F6::
PixelGetColor, colour, curx, cury
Notify("color picker", "coor " . curx . "," . cury . "; color: " . colour)
return

F7::
MouseMove, curx, cury
return

F8::
MouseMove, 0, 0
return

Left::
MouseGetPos, lastx, lasty
lastx -= 1
MouseMove, lastx, lasty
return

Down::
MouseGetPos, lastx, lasty
lasty += 1
MouseMove, lastx, lasty
return

Up::
MouseGetPos, lastx, lasty
lasty -= 1
MouseMove, lastx, lasty
return

Right::
MouseGetPos, lastx, lasty
lastx += 1
MouseMove, lastx, lasty
return
