﻿
; author: haoliang<haoliang0.1.2@gmail.com>
;
; 拾荒者

#IfWinActive ahk_class D3 Main Window Class


#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\cursor.ahk
#Include %A_ScriptDir%\..\lib\function\timing.ahk
#Include %A_ScriptDir%\..\lib\function\salvage.ahk

#Include %A_ScriptDir%\..\lib\function\var.ahk

#Include %A_ScriptDir%\..\lib\keybind\scavenger.ahk


InsightedSatisfy(byref features) {
    missed := []
    for k, v in features {
        PixelGetColor, colour, v[1], v[2]
        if (colour != v[3]) {
            missed[k] := Format("({1},{2}), expected: {3:x}, got: {4:x}", v[1], v[2], v[3], colour)
        }
    }

    if missed.Length() {
        alert := Join(missed)
        MsgBox,, features missed, %alert%
    } else {
        MsgBox,, features satisfied, fine!
    }

    ; (1438,494); expected: 0x3C7ABB; got: [0x102658, ]
}


F2::
global _CONFIRM_

InsightedSatisfy(_CONFIRM_["features"])
return

#IfWinActive