﻿; 公共房间中游荡
;
; key range: F9 - F12

; note:
; * design for {monitor, diablo3} resolution: 2560x1440
; * !z: 游戏按键设定 -- "关闭所有打开的窗口"

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\cursor.ahk
#Include %A_ScriptDir%\..\lib\function\timing.ahk

#Include %A_ScriptDir%\..\lib\function\var.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk


_exitLobby() {
    MouseMoveLeftMiddle()

    x := 289
    y := 633
    colour := 0xFDFDFD

    SendInput {Esc}
    Sleep 250

    if (! FocusSensitive(x, y, colour)) {
        SendInput, !z
        Notify("menu - exit", "features unsatisfied")
        return
    }

    MouseClick, left, x, y
}

_joinLobby() {
    MouseMoveLeftMiddle()

    x := 356
    y := 682
    colour := 0xFDFDFE

    if (! FocusSensitive(x, y, colour)) {
        SendInput, !z
        Notify("menu - join", "features unsatisfied")
        return
    }

    MouseClick, left, x, y
}

_makeLobbyPublic() {
    MouseMoveRightMiddle()

    SendInput {Esc}
    Sleep 250

    x := 2197
    y := 448
    colour := 0xFDFDFD

    if (! FocusSensitive(x, y, colour)) {
        SendInput, !z
        Notify("menu - public", "features unsatisfied")
        return
    }

    MouseClick, left, x, y
    Sleep 250

    MouseMoveTo("bottomMiddle")

    confirmx := 1133
    confirmy := 1049
    confirmcolour := 0xFDFDFD

    if (! FocusSensitive(confirmx, confirmy, confirmcolour)) {
        SendInput, !z
        Notify("menu - public - confirm", "features unsatisfied")
        return
    }

    MouseClick, left, confirmx, confirmy
}

_gotoA1Shelter() {
    ; open map
    SendInput {m}
    ; ensure map opened
    if (! FocusSensitive(1194, 164, 0x00134B)) {
        SendInput, !z
        Notify("go to A1", "features unsatisfied")
        return
    }
    ; go to world map
    MouseClick, left, 1194, 164
    ; go to a1
    MouseClick, left, 978, 819
    ; go to a1.home
    MouseClick, left, 1356, 643
}

; 退出游戏
F9::
_exitLobby()
return

; 加入游戏
F10::
_joinLobby()
return

; 退出游戏+加入游戏
F12::
Gosub, F9
Sleep, 4000
Gosub, F10
Sleep, 1500
Gosub, F10
return

; 设为公共游戏
F11::
_makeLobbyPublic()
return


F8::
Loop {
    Gosub, F12
    Random, duration, 15000, 30000
    Sleep, %duration%
}
return

^Numpad1::
_gotoA1Shelter()
return


#IfWinActive
