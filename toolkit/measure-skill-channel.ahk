﻿
_MARK_ := 0

_mark() {
    global _MARK_ := A_TickCount
}

_reportGap() {
    global _MARK_

    then := _MARK_
    now := A_TickCount

    duration := Format("elapsed: {1:.2f}ms", now - then)

    MsgBox, , 施法前摇时间测量, %duration%
}

return

$s::
_mark()
SendInput {s}
return

f::
_reportGap()
return