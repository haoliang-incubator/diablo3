﻿; author: haoliang<haoliang0.1.2@gmail.com>
;
; 在地图地点中穿梭

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\cursor.ahk
#Include %A_ScriptDir%\..\lib\function\notification.ahk

_openWordmap() {
    ; open map
    SendInput m

    ; ensure map opened

    SendInput, {click right}
}

OpenA1Map() {
    _openWordmap()

    MouseClick, left, 978, 819
}


; 新崔斯特姆
^Numpad1::
OpenA1Map()
; go to a1.home
MouseClick, left, 1356, 643
return


; 获得焦点: 地图上的地点, 箱子分页
; 可激活: 分解按钮
; ui: 工匠、商人、魔盒、衣橱、储藏箱、物品栏、巅峰、个人详细信息、材料、技能、垂直滚动条
; 
; * 多个不同位置的颜色
; * 悬停颜色变换、激活颜色变换

#IfWinActive
