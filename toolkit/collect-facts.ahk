﻿_STASH_ := []

_append() {
    global _STASH_

    _STASH_.Push(_info())
}

_pop() {
    global _STASH_

    _STASH_.Pop()
}

_info() {
    MouseGetPos, x, y
    return [x, y]
}

_line(byref coor) {
    x := coor[1]
    y := coor[2]

    MouseMove, 0, 0
    PixelGetColor, colour, x, y
    MouseMove, x, y, 5
    PixelGetColor, hover, x, y

    return Format("[{1}, {2}, {3}, {4}]", x, y, colour, hover)
}

_legend() {
    return "[x, y, color, hover-color]"
}

_header() {
    FormatTime, now,, yyyy-MM-dd HH:mm:ss

    return Format("### {1} ###", now)
}

_subject() {
    InputBox, subject, , please provide subject for these coordinates
    return Format("subject: {1}", subject)
}


_dump() {
    global _STASH_

    fname := Format("{1}\{2}", A_AppData, "d3.facts")
    file := FileOpen(fname, "a", "UTF-8")
    if not IsObject(file)
        throw, "failed to open file: " . fname

    try {
        file.WriteLine(_header())
        file.WriteLine(_subject())
        file.WriteLine(_legend())

        while _STASH_.Length() {
            v := _STASH_.Pop()
            file.WriteLine(_line(v))
        }
        file.Write("`n")
    } finally {
        file.Close()
    }
}


F4::
_append()
return

F3::
_pop()
return

F1::
_dump()
return
