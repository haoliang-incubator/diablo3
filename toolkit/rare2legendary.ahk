﻿
; presets
; * screen resolution: 2560x1440 or 1920x1080
; * windowed (full screen) d3

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

SetDefaultMouseSpeed, 0

; {x, y} recipe button, {x, y} confirm button, {x, y} left button, {x, y} right button
; "2560x1440"
COORS := {"recipe": [950, 1120], "confirm": [315, 1105], "left": [773, 1120], "right": [1133, 1120]}
; ; "1920x1080"
; COORS := {"recipe": [714, 840], "confirm": [251, 827], "left": [580, 840], "right": [850, 840]}

TOUPGRADES := []
LASTMEMORY := []


UpgradeOne(x, y) {
    global COORS

    ; put item under cursor into the cube
    MouseClick, right, x, y
    ; choose the upgrade recipe
    MouseClick, left, COORS["recipe"][1], COORS["recipe"][2]
    ; confirm upgrade
    MouseClick, left, COORS["confirm"][1], COORS["confirm"][2]
    ; necessary wait
    Sleep, 50

    ; ; way 1
    ; ; collect upgraded item
    ; Sleep, 2350
    ; MouseClick, left, COORS[3], COORS[4]
    ; ; todo: confirm upgrading is succeeded

    ; way 2
    ; by switching recipes to speed up upgrading
    MouseClick, left, COORS["left"][1], COORS["left"][2]
    MouseClick, left, COORS["right"][1], COORS["right"][2]
}

UpgradeAll() {
    global TOUPGRADES
    global LASTMEMORY

    if TOUPGRADES.Length()
        LASTMEMORY := ArrayDeepClone(TOUPGRADES)

    while TOUPGRADES.Length() {
        v := TOUPGRADES.Pop()
        UpgradeOne(v[1], v[2])
    }
}

UpgradeRemembered() {
    global LASTMEMORY

    for _, v in LASTMEMORY {
        UpgradeOne(v[1], v[2])
    }
}

ArrayDeepClone(src) {
    new := []
    for k, v in src {
        new[k] := v
    }
    return new
}

^LButton::
global TOUPGRADES
MouseGetPos, curx, cury
TOUPGRADES.Push([curx, cury])
return

; 分解
!LButton::
Click left
SendInput {Enter}
return

f5::
UpgradeAll()
return

f6::
UpgradeRemembered()
return

; pause the running script, and disable all hotkeys
F4::
Suspend
if A_IsSuspended {
    Pause, on, 1
} else {
    Pause, off, 1
}
return

#IfWinActive