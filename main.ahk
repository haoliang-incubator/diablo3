﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\lib\function\notification.ahk

pid_con := Object()


mutex() {
    global pid_con
    for key, el in pid_con {
        Process, Close, %el%
        pid_con.Delete(key)
    }
}


con_mark(ix, pid) {
    global pid_con
    pid_con[ix] := pid
}


Numpad0::
script_name := "necoe-timer.ahk"
Notify("main", "starting " . script_name)
Run, "%A_ScriptDir%\coe\%script_name%",,Hide,pid
Return


NumpadDiv::
script_name := "scavenger.ahk"
Notify("main", "starting " . script_name)
Run, "%A_ScriptDir%\toolkit\%script_name%",,Hide,pid
Return

NumpadSub::
script_name := "wander.ahk"
Notify("main", "starting " . script_name)
Run, "%A_ScriptDir%\toolkit\%script_name%",,Hide,pid
Return

Numpad1::
script_name := "support_sprint.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\barbarian\%script_name%",,Hide,pid
con_mark(1, pid)
Return


Numpad3::
script_name := "support_stomp.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\barbarian\%script_name%",,Hide,pid
con_mark(3, pid)
Return


Numpad4::
script_name := "support_rift.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\barbarian\%script_name%",,Hide,pid
con_mark(4, pid)
Return


Numpad6::
script_name := "support_s22.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\dh\%script_name%",,Hide,pid
con_mark(6, pid)
Return


Numpad7::
script_name := "90_sprint.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\barbarian\%script_name%",,Hide,pid
con_mark(7, pid)
Return

Numpad8::
script_name := "attack_justice.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\monk\%script_name%",,Hide,pid
con_mark(8, pid)
Return

Numpad9::
script_name := "attack_hunting_arrow.ahk"
Notify("main", "starting " . script_name)
mutex()
Run, "%A_ScriptDir%\dh\%script_name%",,Hide,pid
con_mark(9, pid)
Return


#IfWinActive
