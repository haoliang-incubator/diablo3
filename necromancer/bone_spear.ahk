﻿; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

APP_NAME := "nec: bone spear"

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk

; labels ;{{{
GrimScythe:
Click left
return

BoneSpear:
Click right
return

BoneArmor:
SendInput a
return
;}}}

; hotkeys ;{{{
w::
wToggle := !wToggle
if wToggle {
	SetTimer, GrimScythe, 150
	SetTimer, BoneSpear, off
	SetTimer, BoneArmor, off
} else {
	Gosub, BoneArmor
	SetTimer, BoneArmor, 1000
	SetTimer, BoneSpear, 100
	SetTimer, GrimScythe, off
}
return
;}}}

#IfWinActive
