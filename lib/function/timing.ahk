﻿
; prerequisites:
; * function/cursor.ahk


; n: int
SleepFrames(n) {
    static FRAMETIME := Ceil(1000/60)

    ms := n * FRAMETIME

    Sleep, ms
}

; colors: array [[x, y, color]]
; ftimeout: int, timeout after n frametimes
WaitUntilColorsExist(byref colors, ftimeout) {
    success := false
    loop, %ftimeout% {
        SleepFrames(1)
        if ColorsExist(colors) {
            success := true
            break
        }
    }
    if not success {
        throw, "wait timeout"
    }
}