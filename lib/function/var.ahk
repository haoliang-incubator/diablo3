﻿
; @b bool
; @t mixed; return value if @b is true
; @f mixed; return value if @b is false
Alternative(b, t, f) {
    if b {
        return t
    }

    return f
}

Join(arr, suffix := "`n", prefix := "") {

    tail := arr.Length()
    rest := tail - 1

    if (tail < 1) {
        return ""
    }

    str := ""

    Loop, %rest% {
        ;MsgBox, "#" . %A_Index%
        str .= prefix . arr[A_Index] . suffix
    }

    str .= prefix . arr[tail]

    return str
}

DumpBool(bool) {
    if bool {
        return "true"
    }

    return "false"
}

isArrayEmpty(mixed) {
    if (! isObject(mixed)) {
        return true
    }

    return mixed.Length() < 1
}


FoundInArray(haystack, needle) {
    if isArrayEmpty(haystack)
        return false

    for index, value in haystack
        if (value = needle)
            return true

    return false
}