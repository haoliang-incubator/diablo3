﻿; prerequisites:
; * function/var
;
; note: ; * mouse move speed, can not less than 2 in my test, but will will be different in different machine
;

FocusSensitive(x, y, focusColor) {

    MouseGetPos, cx, cy
    if (cx == x && cy == y) {
        MouseMove, 0, 0
    }

    PixelGetColor, inactiveColor, %x%, %y%
    MouseMove, %x%, %y%, 2
    PixelGetColor, activeColor, %x%, %y%
    MouseMove, %cx%, %cy%, 2

    if (inactiveColor == activeColor) {
        return false
    }

    if (activeColor != focusColor) {
        return false
    }

    return true
}

; colors: array, [[x, y, color]]
ColorsExist(byref colors) {
    for _, v in colors {
        PixelGetColor, colour, v[1], v[2]
        if (colour != v[3]) {
            return false
        }
    }
    return true
}

MouseMoveLeftMiddle() {
    MouseMoveTo("leftMiddle")
}

MouseMoveRightMiddle() {
    MouseMoveTo("rightMiddle")
}

MouseMoveCenter(){
    MouseMoveTo("center")
}

MouseMoveTo(pos := "leftMiddle") {
    static position := { "topMiddle": [A_ScreenWidth/2, 0], "bottomMiddle": [A_ScreenWidth/2, A_ScreenHeight], "leftMiddle": [0, A_ScreenHeight/2], "rightMiddle": [A_ScreenWidth, A_ScreenHeight/2], "center": [A_ScreenWidth/2, A_ScreenHeight/2] }

    co := position[pos]

    if (isArrayEmpty(co)) {
        ; ; warn if can not found position definition
        ; return false
        throw, "unknown position: " . pos
    }

    x := co[1]
    y := co[2]

    MouseMove, %x%, %y%

    return true
}
