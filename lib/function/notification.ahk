﻿; behavior
; * only one notify can exist at the same time

; bug
; * msg that contains 4+ lines, 5 and later line will be discarded.

_MSG_COUNT := 0

; Notify via TrayTip
; @title text
; @msg text, line break: `n
; timeout int, millisecond
Notify(title, msg, timeout := 2000) {
    global _MSG_COUNT += 1

    HideNotify()

    newtitle := Format("{1} #{2}", title, _MSG_COUNT)

    TrayTip, %newtitle%, %msg%,, 16

    SetTimer, HideNotify, -%timeout%
}

HideNotify() {
    TrayTip
    if SubStr(A_OSVersion,1,3) = "10." {
        Menu Tray, NoIcon
        Sleep 200  ; It may be necessary to adjust this sleep.
        Menu Tray, Icon
    }
}