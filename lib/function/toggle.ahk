﻿; specs
;
; * @labels must be an array
;
; * label, must be a LABEL, and must have a global variable named as pattern `<label>Toggle`
; * Label, must handle eToggle == true, false


; todo
; * warning if label is not exist.
; * @labels could be dict

ToggleOn(byref labels) {
    for _, label in labels {
        var := label . "Toggle"
        if (! isLabel(label)) {
            continue
        }
        if (! %var%) {
            Gosub, %label%
        }
    }
}

ToggleOff(byref labels) {
    for _, label in labels {
        var := label . "Toggle"
        if (! isLabel(label)) {
            continue
        }
        if (%var%) {
            Gosub, %label%
        }
    }
}
