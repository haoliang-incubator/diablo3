﻿
; prerequisites:
; * function/timing.ahk
; * function/cursor.ahk

; notes:
; * ui transforming do take times
; * interactive ui will change it's color likely when mouse hovering or clicked


; for 2560x1440
; 6x10箱子 (已知暗黑物品最大占用1x2格, 所以格子坐标最好隔行存放)
_GRIDS_ := [ [2511,1108], [2440,1109], [2374,1108], [2307,1114], [2241,1113], [2171,1108], [2103,1112], [2046,1116], [1972,1110], [1903,1113], [2506,976], [2444,979], [2374,983], [2307,976], [2238,978], [2169,980], [2105,980], [2038,977], [1972,976], [1904,978], [2508,846], [2433,846], [2371,848], [2303,844], [2238,848], [2167,842], [2107,848], [2034,845], [1966,848], [1904,846], [2508,1045], [2436,1049], [2371,1045], [2304,1043], [2236,1043], [2173,1049], [2099,1045], [2039,1044], [1972,1044], [1903,1044], [2509,913], [2438,910], [2373,912], [2310,912], [2238,910], [2168,912], [2104,911], [2039,911], [1974,911], [1908,911], [2509,778], [2440,776], [2373,776], [2308,777], [2237,772], [2168,773], [2101,777], [2037,777], [1968,777], [1898,772] ]
; 确认弹窗
_CONFIRM_ := {"features": [ [1126, 494, 0x55AAF3], [1280, 247, 0x0086C5], [1576, 252, 0x283B49], [983, 250, 0x181C29] ], "yes": [1126, 494]}
; 埃蒙、米丽安、沈老贪
_NPC_ := {"features": [[356, 71, 0x3EB4DA], [383, 85, 0xBF5A1C], [351, 104, 0x60D8DC]]}
; 分解面板
_SALVAGEPANNEL_ := {"coor": [685, 644], "activecolor": 0x1096E4, "inactivecolor": 0x000DA0}
; 分解按钮
_SALVAGEBUTTON_ := {"coor": [209, 334], "activecolor": 0x003640, "inactivecolor": 0x55616B}
; 白蓝黄分解按钮
_WHITEBUTTON_ := {"coor": [330, 357], "activecolor": 0x5C6B75, "inactivecolor": 0x31393E}
_BLUEBUTTON_ := {"coor": [423, 356], "activecolor": 0x586670, "inactivecolor": 0x2F363C}
_YELLOWBUTTON_ := {"coor": [508, 356], "activecolor": 0x626E77, "inactivecolor": 0x343B40}

; ; for 1920x1080
; ; 6x10箱子 (已知暗黑物品最大占用1x2格, 所以格子坐标最好隔行存放)
; _GRIDS_ := [ [1878,833], [1827,832], [1779,831], [1729,832], [1677,831], [1626,831], [1576,835], [1525,833], [1474,831], [1423,830], [1882,735], [1829,733], [1783,733], [1730,734], [1677,735], [1626,732], [1578,731], [1524,733], [1472,731], [1424,733], [1876,629], [1828,630], [1777,631], [1727,633], [1674,631], [1627,631], [1579,630], [1526,630], [1475,628], [1427,633], [1878,783], [1827,783], [1777,783], [1726,783], [1679,782], [1629,781], [1575,782], [1526,783], [1473,782], [1423,780], [1877,682], [1827,685], [1778,683], [1727,684], [1674,684], [1622,685], [1573,680], [1525,683], [1475,683], [1423,680], [1881,581], [1830,580], [1780,581], [1730,581], [1676,582], [1626,582], [1574,582], [1525,579], [1473,580], [1423,582] ]
; ; 确认弹窗
; _CONFIRM_ := {"features": [[847, 370, 0x020514], [1081, 371, 0x3A76B7]], "yes": [847, 370]}
; ; 埃蒙、米丽安、沈老贪
; _NPC_ := {"features": [[244, 64, 0xB4511F], [263, 76, 0x63DADC], [266, 53, 0x216EAB]]}
; ; 分解面板
; _SALVAGEPANNEL_ := {"coor": [515, 485], "activecolor": 0x15C0FC, "inactivecolor": 0x000DA5}
; ; 分解按钮
; _SALVAGEBUTTON_ := {"coor": [153, 247], "activecolor": 0x0, "inactivecolor": 0x64707A}
; ; 白蓝黄分解按钮
; _WHITEBUTTON_ := {"coor": [247, 267], "activecolor": 0x63727A, "inactivecolor": 0x353D41}
; _BLUEBUTTON_ := {"coor": [314, 268], "activecolor": 0x5B666C, "inactivecolor": 0x30363A}
; _YELLOWBUTTON_ := {"coor": [379, 266], "activecolor": 0x727D8B, "inactivecolor": 0x3D434


SalvageAll() {
    global _WHITEBUTTON_, _BLUEBUTTON_, _YELLOWBUTTON_

    ; 打开分解页面
    _openSalvagePannel()

    ; 分解箱中
    ; * 所有白装
    _salvageShortcut(_WHITEBUTTON_["coor"][1], _WHITEBUTTON_["coor"][2], _WHITEBUTTON_["inactivecolor"], _WHITEBUTTON_["activecolor"])
    ; * 所有蓝装
    _salvageShortcut(_BLUEBUTTON_["coor"][1], _BLUEBUTTON_["coor"][2], _BLUEBUTTON_["inactivecolor"], _BLUEBUTTON_["activecolor"])
    ; * 所有黄装
    _salvageShortcut(_YELLOWBUTTON_["coor"][1], _YELLOWBUTTON_["coor"][2], _YELLOWBUTTON_["inactivecolor"], _YELLOWBUTTON_["activecolor"])
    ; * 单个传奇/套装
    _salvageLegendaries()
}


; x, y: int
; inactive, active: hex
_salvageShortcut(x, y, inactive, active) {
    MouseMove, 0, 0, 0
    PixelGetColor, colour, x, y
    switch colour {
        case inactive:
            ; do nothing
        case active:
            MouseClick, left, x, y, 1, 0
            _confirm(true, 10)
        default:
            Throw, Format("salvage shortcut error: ({1}, {2}, {3})", x, y, colour)
    }
}

; mandatory: bool
; ftimeout: int, wait for n frametime
_confirm(mandatory, ftimeout) {
    global _CONFIRM_

    ; safe position which will not cause hover/focus event
    MouseMove, A_ScreenWidth/2, A_ScreenHeight/2, 0

    needAnswer := true

    try {
        WaitUntilColorsExist(_CONFIRM_["features"], ftimeout)
    } catch e {
        needAnswer := false
        if mandatory {
            throw e
        }
    }

    if needAnswer {
        ; global _CONFIRM_
        ; x := _CONFIRM_["yes"][1]
        ; y := _CONFIRM_["yes"][2]
        ; MouseClick, left, x, y, 1, 0

        SendInput, {enter}
        SleepFrames(1)
    }
}

_npcOpened() {
    global _NPC_

    return ColorsExist(_NPC_["features"])
}

_openSalvagePannel() {
    global _SALVAGEPANNEL_

    if not _npcOpened() {
        Throw, "blacksmith dialog did not open"
    }

    x := _SALVAGEPANNEL_["coor"][1]
    y := _SALVAGEPANNEL_["coor"][2]
    active := _SALVAGEPANNEL_["activecolor"]
    inactive := _SALVAGEPANNEL_["inactivecolor"]

    MouseMove, 0, 0, 0
    PixelGetColor, colour, x, y
    switch colour {
        case inactive:
            MouseClick, left, x, y, 1, 0
        case active:
            ; do nothing
        default:
            Throw, "failed to open salvage pannel"
    }

    SleepFrames(1)
}

_enterSalvageMode() {
    global _SALVAGEBUTTON_

    x := _SALVAGEBUTTON_["coor"][1]
    y := _SALVAGEBUTTON_["coor"][2]
    active := _SALVAGEBUTTON_["activecolor"]
    inactive := _SALVAGEBUTTON_["inactivecolor"]

    MouseMove, 0, 0, 0
    PixelGetColor, colour, x, y
    switch colour {
        case inactive:
            MouseClick, left, x, y, 1, 0
        case active:
            ; do nothing
        default:
            Throw, "failed to enter salvage mode"
    }

    SleepFrames(1)
}

_exitSalvageMode() {
    ; todo ensure in salvage mode
    SendInput, {click right}
}

_salvageLegendaries() {
    global _GRIDS_

    _enterSalvageMode()

    ; leaks := []

    for _, v in _GRIDS_ {
        PixelGetColor, colour, v[1], v[2]
        switch colour {
            case 0x080D10, 0x080D0D, 0x080E10, 0x080B0B, 0x080F10, 0x080C0C, 0x080F0E, 0x0B0E10, 0x0C0E10, 0x0B0D10, 0x090D10, 0x0B0F0D, 0x0A0E10:
                continue
            default:
                ; leaks.Push([v[1], v[2], colour])
                ; do nothing
        }

        MouseClick, left, v[1], v[2], 1, 0
        _confirm(false, 2)
    }

    ; _recordLeakedGrids(leaks)

    _exitSalvageMode()
}

; leaks: array [[x, y, color]]
_recordLeakedGrids(byref leaks) {
    if not leaks.Length() {
        return
    }

    while leaks.Length() {
        v := leaks.Pop()

        leakey := v[1] . ":" . v[2]
        leakval := v[3]
        file := A_AppData . "\d3assist.ini"

        IniWrite, %leakval%, %file%, inventory-leaked, %leakey%
    }
}
