﻿
; buyer aware: you should keep the count be increased every time you changing transmission speed
;
; @count int, called times
; @mods array, kinds of transmission speed
TransmissionSpeed(count, mods) {
    return mods[Mod(count, mods.Length()) + 1]
}
