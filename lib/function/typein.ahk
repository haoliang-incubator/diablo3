﻿

; @msg string, but ascii characer only
TypeIn(msg) {
    if StrLen(msg) < 1 {
        return
    }

    Pause On, 1

    SendInput {Enter}%msg%{Enter}

    Pause Off
}

TypeInUnicode(msg) {
    if StrLen(msg) < 1 {
        return
    }

    Pause On, 1

    SendInput {Enter}
    STDOUT(msg)
    SendInput {Enter}

    Pause Off
}

; @writeIn string, unicode character supported
;
; see https://stackoverflow.com/a/16035538
;
STDOUT(writeIn) {

    ; origin := ClipBoardAll

    ; copy to clipboard
    Clipboard := writeIn
    while (Clipboard != writeIn)
        Sleep, 10

    ; copy to stdout(?)
    Send ^v
    ; Generous, less wait or none will often work.
    Sleep, 0

    ; ; possible race condition when ^v do not complete immediately and we restore clipboard directly
    ; ; see https://www.autohotkey.com/boards/viewtopic.php?f=76&t=63356&start=20
    ;
    ; ; restore origin clipboard
    ; Clipboard := origin
    ; ; free memory
    ; origin := ""
}
