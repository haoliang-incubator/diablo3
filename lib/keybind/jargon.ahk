﻿; provide some 暗号/战斗术语

; reauires lib/function/typein

_JARGON_COUNTER := 0

_jargonMark() {

    ; FormatTime, now,, m:s
    ; return "(" . now . ")"

    global _JARGON_COUNTER += 1
    return " #" . _JARGON_COUNTER
}

1::
TypeInUnicode("111")
return

2::
TypeInUnicode("去下层吧")
return

3::
TypeInUnicode("333")
return

4::
TypeInUnicode("豆角**带**下残血精英")
return

5::
TypeInUnicode("豆角来**收**残血")
return

6::
TypeInUnicode("打波小怪" . _jargonMark())
return

7::
TypeInUnicode("准备开塔了")
return

8::
TypeInUnicode("有霸王" . _jargonMark())
return

9::
TypeInUnicode("稍等，附近还有精英" . _jargonMark())
return

0::
TypeInUnicode("下轮元素回来踩")
return