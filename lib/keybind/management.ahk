﻿; key range: F1 - F4

; prerequisites
; * function/notification.ahk

F1::
if APP_NAME {
    msg := "name: " . APP_NAME
} else {
    msg := "name: " . "unknown"
}
Notify("script management", msg)
return

; pause the running script, and disable all hotkeys
F4::
Suspend
if A_IsSuspended {
    Pause, on, 1
} else {
    Pause, off, 1
}
if A_IsSuspended {
    Notify("script management", "suspend & pause")
} else {
    Notify("script management", "unsuspend & unpause")
}
return

; exit this script
; exit operation should be more intenional, so did not bind to F*
^q::
Notify("script management", "exiting")
ExitApp
return