﻿; requires lib\function\{toggle,var}.ahk

; startup
F5:: ;{{{

startupToggle := !startupToggle

if (isArrayEmpty(APP_STARTUP_LABELS)) {
    Notify("startup", "lack of APP_STARTUP_LABELS definition.")
    return
}

; first run, startupToggle == true
if startupToggle {
    Notify("startup", "initializing")
    ToggleOn(APP_STARTUP_LABELS)
} else {
    Notify("startup", "relieving")
    ToggleOff(APP_STARTUP_LABELS)
}
return ;}}}
