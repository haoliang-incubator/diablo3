﻿
; prerequisites:
; * function/salvage.ahk
;     * function/timing.ahk
;     * function/cursor.ahk

_dropitem() {
    MouseGetPos, curx, cury
    tox := A_ScreenWidth/2
    toy := A_ScreenHeight/2

    MouseClickDrag, left, curx, cury, tox, toy, 0
    MouseMove, curx, cury
}

; 扔下
!LButton::
_dropitem()
return

F3::
SalvageAll()
return

^LButton::
SendInput {click left}{enter}
return
