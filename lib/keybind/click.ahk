﻿
;{{{ labels 

ClickLeft:
Click left
return

ClickRight:
Click right
return

;}}}

q::
Goto, WheelUp
return

; keep left-click
WheelUp:: ;{{{

; we may change qToggle rather than WheelUpToggle by ToggleOn/Off
WheelUpToggle := qToggle
; switch
WheelUpToggle := !WheelUpToggle
; keep both same
qToggle := WheelUpToggle

; mutually exclusive with `WheelDown`
if (WheelUpToggle && WheelDownToggle) {
    Gosub, WheelDown
}

if WheelUpToggle {
    SetTimer, ClickLeft, 50
} else {
    SetTimer, ClickLeft, off
}

return ;}}}

; keep right-click
WheelDown:: ;{{{

WheelDownToggle := !WheelDownToggle

; mutually exclusive with `WheelUp`
if (WheelDownToggle && WheelUpToggle) {
    Gosub, WheelUp
}

if WheelDownToggle {
    SetTimer, ClickRight, 50
} else {
    SetTimer, ClickRight, off
}

return ;}}}
