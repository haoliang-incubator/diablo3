"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FileName: nvimrc
" Maintainer: haoliang [haoliang0.1.2@gmail.com]

syntax enable
filetype plugin indent on
let mapleader=" "
let g:mapleader=" "

" self {{{

" vim Keyword Help
"{{{
set keywordprg="help"
"}}}

" vim backup && history
"{{{
set nobackup
"set backupdir  = $HOME/.vim/files/backup
"set backupext  = -vimbackup
"set backupskip =
set nowritebackup
set noswapfile
"set directory = $HOME/.vim/files/swap/
set undofile
"set undodir=$HOME/.vim/files/undo/
set undolevels=1000
set undoreload=1000
set updatecount=100
set history=5000
"set clipboard = unnamed " use clipboard
" }}}

" vim search
"{{{
set magic
set ignorecase
set smartcase
set hlsearch
set incsearch
"set wrapscan
" }}}

" vim session && viminfo
"{{{
" Return to last edit position when opening files
autocmd BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \   exe "normal! g`\"" |
            \ endif
set sessionoptions+=blank,buffers,curdir,folds,help,options,winsize,resize
set sessionoptions+=unix,slash
"}}}

" vim mouse && cursor
"{{{
behave xterm
set mouse=a
set scrolloff=0
"set cursorline
"highlight CursorLine " guibg=#003853 ctermbg=24  gui=none cterm=none
"}}}

" vim encode
"{{{
set encoding=utf8
set bomb
let &termencoding=&encoding
set fileencoding=utf-8
set fileencodings=utf-8,ucs-bom,gbk,cp936,big5,latin-1
set ff=unix
set ffs=unix
" }}}

" vim fold
"{{{
"set nofoldenable
set foldmethod=marker
set nofoldenable
set foldcolumn=0
set foldlevel=0
" }}}

" vim line display
"{{{
set nowrap
set linebreak
"set breakindent
"set breakindentopt=min:40
set textwidth=78
set linespace=5
set formatoptions=tcqn2vlB1j
set display=lastline
set nolist
" 一些符号
" ∮∝ ∞ ∥ ⊙ ∽ ∈ ∵ ∴ √ ∷ § ? ? ? ? ? ● ? ? ? ? ? ?
"}}}

" vim tab
"{{{
set softtabstop=4
set tabstop=4
set shiftround
set shiftwidth=4
set smarttab
set expandtab
"}}}

" vim status line
"{{{
set laststatus=2
set cmdheight=1
set statusline=\ %<%F%h%m%r%=%b\ 0x%B\ %y\ %0(%{&fileformat}\ [%{(&fenc==\"\"?&enc:&fenc).(&bomb?\",BOM\":\"\")}]\ %c:%l/%L%)
" }}}

" vim complete
"{{{
" dictionary
"set complete-=k complete+=k
"set omnifunc=syntaxcomplete#Complete
set completeopt=menu
set wildmode=list:full
set wildmenu
set more
" for vim
set wildignore+=*~,*.bak,*.un~,tags
" for git
"set wildignore+=*/.git/* " disable for fugitive
" for yii
set wildignore+=*/runtime/*,*/web/assets/*,*/web/css/*,*/web/js/*,*/web/images/*,*/web/uploads/*
set wildignore+=*/migrations/*,*/environments/*
"set wildignore+=*/tests/*
"set wildignore+=*/docker/*
set suffixes=.un~,.bak,~,.swp,.log,.data
"for `find ./vendor -type f | sed 's/.*\///' | grep '\.' | sed 's/.*\.//' | sort | uniq -c | sort -h`
set wildignore+=*.data,*.sample
set wildignore+=*.css,*.scss,*.less
set wildignore+=*.js
set wildignore+=*.jpg,*.gif,*.png,*.psd
set wildignore+=*.htmlt
" }}}

" vim spell check
"{{{
"set spell spelllang=en_us
"}}}

" vim intent
"{{{
set autoindent
"set copyindent
"set cindent
"set smartindent
"set preserveindent
"}}}

" vim UI
"{{{
"colorscheme spongebob
set colorcolumn=0
" }}}

" vim tag
"{{{
"set notagrelative
set tagstack
"}}}

" number
"{{{
set number
set relativenumber
set numberwidth=2
"}}}

" vim MISC
"{{{
set modeline
set modelines=5
set noshowmode
set shortmess=atl
"set autochdir
"set ruler
"set rulerformat=%15(%c%V\ %p%%%)
"set backspace=eol,start,indent " backspack is a bad habit
set sidescroll=20
" some charactor doesn't work in multi-line: <>,[], b,s, h,l, ~
"set whichwrap+=b,s
set noautoread
set hidden
"set lazyredraw
set showmatch
set showmode
set visualbell
"set maxmem=100000 " k
set pastetoggle =<F4>
set splitbelow
set splitright
set timeout
set ttimeout
set timeoutlen=1000
set ttimeoutlen=1000
set ttyfast
"set virtualedit=block,onemore
" }}}

" {quick,local}-list {{{
nnoremap ( :lprevious<CR>
nnoremap ) :lnext<CR>
"}}}

" gui
"{{{
set guioptions=""
"}}}

" }}}

" map {{{

" mapping pair quote
"{{{
"nnoremap <leader><leader> %i <esc>%a <esc>l
" }}}

" mapping search && substitute
"{{{
" 在选择区域搜索
nnoremap \g/ <ESC>ggVG<ESC>/\%V
"vnoremap <leader>/ <ESC>/\%V
"nnoremap \/ /\c
nnoremap <leader>/ :let @/ = ""<CR>
" highlight public property
command! Hp /\c\v^\s*(public|protected|private|static)[^(;:]+\$\zs\w+\ze
" highlight method
command! H /\c\v^\s*(public|protected|private|static)[^(]+function\s+\zs\w+\ze
" highlight public method
command! Hpub /\c\v^\s*(public|static)[^(]+function\s+\zs\w+\ze
" 必须以action开始且长度大于两个字符的方法
command! Ha /\c\v^\s*(public|protected|private)[^(]+function\s+action\zs\w{2,}\ze
" 高亮git-diff标签
command! Hd /\v[<>=|]{7}
" }}}

" mapping bash like
"{{{
cnoremap <C-A>      <Home>
cnoremap <C-E>      <End>
" 与 tmux 快捷键冲突
"cnoremap <C-K>      <C-U>
cnoremap <c-p>      <Up>
cnoremap <c-n>      <Down>
" }}}

" mapping buffer
"{{{
"nnoremap <leader>ba :1,300 bd!<CR>
"nnoremap <c-n> :bnext<CR>
"nnoremap <c-p> :bprev<CR>
"}}}

" mapping info iab
"{{{
"iab xdate <c-r>=strftime("%c")<CR>
" }}}

" mapping switcher between window
"{{{
nnoremap <c-l> <c-w>l
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
"nnoremap <tab> <c-w>w " it conflit to <c-i> which stands for advance
inoremap <c-l> <del>
nnoremap <tab> gt
nnoremap <s-tab> gT
"}}}

"mapping tidy white-character
"{{{
" 尾部多余空白符
command! Ts %s/\s\+$//e
" 多余空行
command! Tl %s/^\s*\n\s*$//e
" windows下的
command! Tm %s/\r//e
" 变tab为4个空格
command! Tt %s/\t/    /ge
"}}}

" mapping control window size
"{{{
command! Wi vertical res +10
command! Wx vertical res -10
command! Hi res +10
command! Hx res -10
"}}}

" mapping change default map
"{{{
nnoremap 0 ^
nnoremap ^ 0
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
"}}}

" mapping MISC
"{{{
nnoremap <leader>p "+p
" 以sudo方式保存文件
command! W w !sudo tee % > /dev/null
" 快速结尾
nnoremap <leader>; A;<esc>
nnoremap <leader>, A,<esc>
" 搜索类中方法
nnoremap <leader>j /\c\v^\s*(public\|protected\|private)?(\s+static)?\s+function\s+(action)?\zs\ze<LEFT><LEFT><LEFT>
" :find
nnoremap <leader>e :e <C-R>=expand("%:p:h") . "/"<CR>
nnoremap <leader>c :w <C-R>=expand("%:p:h") . "/"<CR>
nnoremap Y y$
"}}}

" }}}
