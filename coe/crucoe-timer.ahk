﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk

#Include %A_ScriptDir%\lib\crucoe.ahk

SetTimer, Report, 1000

Report:
CrucoeStateReport()
return

!1::
CrucoeSinceReset(1)
Notify("crucoe-timer", "计时于元素 火焰")
return

!2::
CrucoeSinceReset(2)
Notify("crucoe-timer", "计时于元素 神圣")
return

!3::
CrucoeSinceReset(3)
Notify("crucoe-timer", "计时于元素 闪电")
return

!4::
CrucoeSinceReset(4)
Notify("crucoe-timer", "计时于元素 物理")
return

!+q::
Notify("crucoe-timer", "exiting")
ExitApp
return

!Xbutton1::
; backward
CrucoeSinceTune(1)
Notify("crucoe-timer", "延后1秒")
return

!Xbutton2::
; forward
CrucoeSinceTune(-1)
Notify("crucoe-timer", "提前1秒")
return

!0::
barbStomp := "s"

Notify("crucoe-timer", "神圣踩踏(" . barbStomp . ")")
KeyWait, %barbStomp%, D T15
if ErrorLevel {
    ; timeout reached
    return
}

CrucoeSinceReset(2-1/4/2)
Notify("crucoe-timer", "计时于元素 神圣")
return


#IfWinActive