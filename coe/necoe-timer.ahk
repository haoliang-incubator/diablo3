﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk

#Include %A_ScriptDir%\lib\necoe.ahk

SetTimer, Report, 1000

Report:
NecoeStateReport()
return

^1::
NecoeSinceReset(1)
Notify("necoe-timer", "计时于元素 冰霜")
return

^2::
NecoeSinceReset(2)
Notify("necoe-timer", "计时于元素 物理")
return

^3::
NecoeSinceReset(3)
Notify("necoe-timer", "计时于元素 毒性")
return

XButton1::
; backward
NecoeSinceTune(1)
Notify("necoe-timer", "延后1秒")
return

Xbutton2::
; forward
NecoeSinceTune(-1)
Notify("necoe-timer", "提前1秒")
return

^+q::
Notify("necoe-timer", "exiting")
ExitApp
return

^0::
barbStomp := "s"

Notify("necoe-timer", "毒性踩踏(" . barbStomp . ")")
KeyWait, %barbStomp%, D T15
if ErrorLevel {
    ; timeout reached
    return
}

NecoeSinceReset(3-1/4/2)
Notify("necoe-timer", "计时于元素 毒性")
return

MButton::
Goto, ^0
return


#IfWinActive