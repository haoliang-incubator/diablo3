﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk

#Include %A_ScriptDir%\lib\wizcoe.ahk

SetTimer, Report, 1000

Report:
WizcoeStateReport()
return

^1::
WizcoeSinceReset(1)
Notify("wizcoe-timer", "计时于元素 奥数")
return

^2::
WizcoeSinceReset(2)
Notify("wizcoe-timer", "计时于元素 冰霜")
return

^3::
WizcoeSinceReset(3)
Notify("wizcoe-timer", "计时于元素 火焰")
return

^4::
WizcoeSinceReset(4)
Notify("wizcoe-timer", "计时于元素 闪电")
return

XButton1::
; backward
WizcoeSinceTune(1)
Notify("wizcoe-timer", "延后1秒")
return

Xbutton2::
; forward
WizcoeSinceTune(-1)
Notify("wizcoe-timer", "提前1秒")
return

^+q::
Notify("wizcoe-timer", "exiting")
ExitApp
return

^0::
barbStomp := "s"

Notify("wizcoe-timer", "毒性踩踏(" . barbStomp . ")")
KeyWait, %barbStomp%, D T15
if ErrorLevel {
    ; timeout reached
    return
}

WizcoeSinceReset(3-1/4/2)
Notify("wizcoe-timer", "计时于元素 火焰")
return

MButton::
Goto, ^0
return


#IfWinActive