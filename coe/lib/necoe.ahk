﻿; NECromancer CoE state
; CoE: Convention of Elements
; 死灵的元素戒状态流转: 冰霜 -> 物理 -> 毒性 [-> 冰霜]
; 豆角: 火焰 -> 神圣 -> 闪电 -> 物理 [-> 火焰]
; 每个元素持续4秒

NECOE_STATES := ["冰霜", "物理", "毒性"]
NECOE_SINCE := 0

; return int, unit milliseconds
CurrentTime(){
    return Ceil(A_TickCount)
}

; return int
CurrentState(byref index, byref clock) {
    global NECOE_SINCE
    global NECOE_STATES

    now := CurrentTime()
    len := NECOE_STATES.Length()
    total := 4 * len

    clock := Mod( (now - NECOE_SINCE) // 1000, total)
    ; index of array in autohotkey starts from 1
    index := (clock // 4) + 1
}

; target int, index of NECO_STATES
NecoeSinceReset(target := 1) {
    current := CurrentTime()
    offset := ceil((target - 1 ) * 4 * 1000)

    global NECOE_SINCE := current - offset
}

; offset int
NecoeSinceTune(offset) {
    global NECOE_SINCE += ceil(offset * 1000)
}

NecoeStateReport() {
    global NECOE_STATES

    index := 1
    clock := 0
    CurrentState(index, clock)

    hint := "#" . (clock + 1) . ":" . NECOE_STATES[index] 
    title := "nec-coe"

    ; silver 时间还早
    ; yellow 准备阶段
    ; lime 爆发阶段

    y := 200

    if (index == 1) {
        Progress, y%y% b zh0 fs22 ws700 ctsilver cwffffff, %hint%, , %title%
    } else if (index == 2) {
        Progress, y%y% b zh0 fs22 ws700 ctyellow cwffffff, %hint%, , %title%
    } else if (index == 3) {
        Progress, y%y% b2 zh0 fs28 ws900 ctlime cwffffff, %hint%, , %title%
    }

    WinSet, AlwaysOnTop, On, %title%
    WinSet, Disable,, %title%
    WinSet, TransColor, ffffff, %title%
    Sleep, 950
    Progress, Off
}