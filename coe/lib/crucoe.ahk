﻿; CRUsader Convention of Elements state
; 死灵的元素戒状态流转: 冰霜 -> 物理 -> 毒性 [-> 冰霜]
; 豆角: 火焰 -> 神圣 -> 闪电 -> 物理 [-> 火焰]
;
; conflicts with function/necoe.ahk
;
; todo merge with function/necoe.ahk

CRUCOE_STATES := ["火焰", "神圣", "闪电", "物理"]
CRUCOE_SINCE := 0

; return int
CurrentTime(){
    return Ceil(A_TickCount)
}

; return int
CurrentState(byref index, byref clock) {
    global CRUCOE_SINCE
    global CRUCOE_STATES

    now := CurrentTime()
    len := CRUCOE_STATES.Length()
    total := 4 * len

    clock := Mod( (now - CRUCOE_SINCE) // 1000, total)
    ; index of array in autohotkey starts from 1
    index := (clock // 4) + 1

    return index
}

CrucoeSinceReset(target := 1) {
    current := CurrentTime()
    offset := ceil((target - 1 ) * 4 * 1000)

    global CRUCOE_SINCE := current - offset
}

; offset int
CrucoeSinceTune(offset) {
    global CRUCOE_SINCE += ceil(offset * 1000)
}

CrucoeStateReport() {
    global CRUCOE_STATES

    index := 1
    clock := 0
    CurrentState(index, clock)

    hint := "$" . (clock + 1) . ":" . CRUCOE_STATES[index] 
    title := "cru-coe"

    ; silver 时间还早
    ; yellow 准备阶段
    ; lime 爆发阶段

    if (index == 1) {
        Progress, y250 b zh0 fs22 ws700 ctyellow cwffffff, %hint%, , %title%
    } else if (index == 2) {
        Progress, y250 b2 zh0 fs28 ws900 ctlime cwffffff, %hint%, , %title%
    } else if (index == 3) {
        Progress, y250 b zh0 fs22 ws700 ctyellow cwffffff, %hint%, , %title%
    } else if (index == 4) {
        Progress, y250 b2 zh0 fs28 ws900 ctlime cwffffff, %hint%, , %title%
    }

    WinSet, AlwaysOnTop, On, %title%
    WinSet, Disable,, %title%
    WinSet, TransColor, ffffff, %title%
    Sleep, 950
    Progress, Off
}