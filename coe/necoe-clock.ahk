﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk

NECOE_SINCE := CurrentTime()
HI_AXIS := 9 ; [1-4, 5-8, 9-12]

; return int
CurrentTime(){
    return Floor(A_TickCount/1000)
}

; return int [1, 12]
CurrentStateClock() {
    global NECOE_SINCE

    now := CurrentTime()
    len := 12

    ; index of array in autohotkey starts from 1
    return Mod(now - NECOE_SINCE, len) + 1
}

; ref int, point int
CircularPosition(ref, point){
    ; return upcoming, during, faraway

    ; min := 1 ; start from 1 not 0
    max := 12
    unit := 4

    ; situation ref=2
    ; [6-7-8-9, 10-11-12-1, 2-3-4-5]
    if (ref < unit) {
        if (point < ref or point >= max - (unit - ref)) {
            return "upcoming"
        }
        if (point >= ref and point < ref + unit) {
            return "during"
        }
        return "faraway"
    }

    ; situation ref=11
    ; [3-4-5-6, 7-8-9-10, 11-12-1-2]
    if (ref > max - unit + 1) {
        if (point < ref and point >= ref - unit) {
            return "upcoming"
        }
        if (point >= ref) {
            return "during"
        }
        if (point >= 1 and point <= unit - (max - ref) - 1) {
            return "during"
        }
        return "faraway"
    }

    ; situation ref=9
    ; [1-4, 5-8, 9-12] => [faraway, upcoming, during]
    if (point < ref and point >= ref - unit) {
        return "upcoming"
    }
    if (point >= ref and point < ref + unit) {
        return "during"
    }
    return "faraway"
}


ClockReport(){
    global HI_AXIS

    clock := CurrentStateClock()

    hint := clock
    title := "necoe-clock"

    switch CircularPosition(HI_AXIS, clock) {
        case "upcoming":
            Progress, y200 b zh0 fs32 ws900 ctyellow cwffffff, %hint%, , %title%
        case "during":
            Progress, y200 b zh0 fs38 ws1000 ctlime cwffffff, %hint%, , %title%
        case "faraway":
            Progress, y200 b zh0 fs32 ws700 ctsilver cwffffff, %hint%, , %title%
    }

    WinSet, AlwaysOnTop, On, %title%
    WinSet, Disable,, %title%
    WinSet, TransColor, ffffff, %title%
    Sleep, 950
    Progress, Off
}

HighlightNow(offset := 0){
    current := CurrentStateClock()
    len := 12

    ; no +1
    global HI_AXIS := Mod(current + offset, len)
}

; offset int
HighlightTune(offset := 0){
    global HI_AXIS := HI_AXIS + offset
}


; daemon
setTimer, Report, 1000


Report:
ClockReport()
return


^1::
HighlightNow(8)
Notify("necoe-clock", "当前：冰霜")
return

^2::
HighlightNow(4)
Notify("necoe-clock", "当前：物理")
return

^3::
HighlightNow(0)
Notify("necoe-clock", "当前：毒性")
return

XButton1::
HighlightTune(1)
Notify("necoe-clock", "延后1秒")
return

XButton2::
HighlightTune(-1)
Notify("necoe-clock", "提前1秒")
return

#IfWinActive