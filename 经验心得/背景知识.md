
source: bbs.d.168.com, maxroll.gg


* [暗黑地图学](https://bbs.d.163.com/forum.php?mod=viewthread&tid=173611089&extra=&page=1)
* [大秘境怪物手册](https://bbs.d.163.com/forum.php?mod=viewthread&tid=173756097)
* [大小米难度](https://bbs.d.163.com/forum.php?mod=viewthread&tid=173568467&fromuid=2554796)
* [暗黑3基础机制](https://bbs.d.163.com/forum.php?mod=viewthread&tid=173551075)
* 怪物群控抗性
    * https://bbs.d.163.com/forum.php?mod=viewthread&tid=173553201




地貌:

I

* 墓穴
* 苦痛**大厅**
* 烂木林
* 蜘蛛洞
* 大**教堂**

II
* 先民**神殿**
* 迷雾**荒原**
* 烈风之地: 沙漠

III
* 亚瑞特核心/巨坑/地狱裂隙: 地核
* 要塞深渊: 兵营

IV
* 银色高塔/遭围高塔: 银光、高塔

V
* 科乌斯废墟: 遗迹
* 永恒**战场**
* 威斯特玛上**城区**

?
* IV 下层炼狱
* V **混沌要塞**

* 放逐之境: 悬崖
* 兵营
* 被遗忘的废墟: 废墟