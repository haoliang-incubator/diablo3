﻿
dict := {}

if (dict.foo) {
    MsgBox, dict.foo is true
} else {
    MsgBox, dict.foo is false
}

dict.bar := true

if (dict.bar) {
    MsgBox, dict.bar is true
} else {
    MsgBox, dict.bar is false
}
