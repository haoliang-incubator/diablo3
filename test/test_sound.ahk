﻿
^1::
SoundPlay, *-1
return

^2::
SoundPlay, *16
return

^3::
; default
SoundBeep, 523, 150
return

^4::
SoundBeep, 32767, 150
return

^5::
SoundBeep, 37, 150
return

^6::
SoundBeep, 2000, 20
return
