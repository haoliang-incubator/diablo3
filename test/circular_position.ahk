﻿; ref int, point int
CircularPosition(ref, point){
    ; return upcoming, during, faraway

    ; min := 1 ; start from 1 not 0
    max := 12
    unit := 4

    ; situation ref=2
    ; [6-7-8-9, 10-11-12-1, 2-3-4-5]
    if (ref < unit) {
        if (point < ref or point >= max - (unit - ref)) {
            return "upcoming:1"
        }
        if (point >= ref and point < ref + unit) {
            return "during:1"
        }
        return "faraway:1"
    }

    ; situation ref=11
    ; [3-4-5-6, 7-8-9-10, 11-12-1-2]
    if (ref > max - unit + 1) {
        if (point < ref and point >= ref - unit) {
            return "upcoming:2"
        }
        if (point >= ref) {
            return "during:2"
        }
        if (point >= 1 and point <= unit - (max - ref) - 1) {
            return "during:2"
        }
        return "faraway:2"
    }

    ; situation ref=9
    ; [1-4, 5-8, 9-12] => [faraway, upcoming, during]
    if (point < ref and point >= ref - unit) {
        return "upcoming:3"
    }
    if (point >= ref and point < ref + unit) {
        return "during:3"
    }
    return "faraway:3"
}

assertTrue(ref, point, position) {
    actual := CircularPosition(ref, point)
    if (actual != position) {
        Throw, "ref=" . ref . ", point=" . point . "; expects position=" . position . ", actual=" . actual
    }
}

; basic, ref=9
assertTrue(9, 1, "faraway:3")
assertTrue(9, 2, "faraway:3")
assertTrue(9, 3, "faraway:3")
assertTrue(9, 4, "faraway:3")
assertTrue(9, 5, "upcoming:3")
assertTrue(9, 6, "upcoming:3")
assertTrue(9, 7, "upcoming:3")
assertTrue(9, 8, "upcoming:3")
assertTrue(9, 9,  "during:3")
assertTrue(9, 10, "during:3")
assertTrue(9, 11, "during:3")
assertTrue(9, 12, "during:3")

; basic, ref=11
assertTrue(11, 3, "faraway:2")
assertTrue(11, 4, "faraway:2")
assertTrue(11, 5, "faraway:2")
assertTrue(11, 6, "faraway:2")
assertTrue(11, 7,  "upcoming:2")
assertTrue(11, 8,  "upcoming:2")
assertTrue(11, 9,  "upcoming:2")
assertTrue(11, 10, "upcoming:2")
assertTrue(11, 11, "during:2")
assertTrue(11, 12, "during:2")
assertTrue(11, 1,  "during:2")
assertTrue(11, 2,  "during:2")

; basic, ref=2
assertTrue(2, 2, "during:1")
assertTrue(2, 3, "during:1")
assertTrue(2, 4, "during:1")
assertTrue(2, 5, "during:1")
assertTrue(2, 6, "faraway:1")
assertTrue(2, 7, "faraway:1")
assertTrue(2, 8, "faraway:1")
assertTrue(2, 9, "faraway:1")
assertTrue(2, 10, "upcoming:1")
assertTrue(2, 11, "upcoming:1")
assertTrue(2, 12, "upcoming:1")
assertTrue(2, 1,  "upcoming:1")

; addition, ref=12
assertTrue(12, 4, "faraway:2")
assertTrue(12, 5, "faraway:2")
assertTrue(12, 6, "faraway:2")
assertTrue(12, 7,  "faraway:2")
assertTrue(12, 8,  "upcoming:2")
assertTrue(12, 9,  "upcoming:2")
assertTrue(12, 10, "upcoming:2")
assertTrue(12, 11, "upcoming:2")
assertTrue(12, 12, "during:2")
assertTrue(12, 1,  "during:2")
assertTrue(12, 2,  "during:2")
assertTrue(12, 3,  "during:2")

; addition, ref=3
assertTrue(3, 7,  "faraway:1")
assertTrue(3, 8,  "faraway:1")
assertTrue(3, 9,  "faraway:1")
assertTrue(3, 10, "faraway:1")
assertTrue(3, 11, "upcoming:1")
assertTrue(3, 12, "upcoming:1")
assertTrue(3, 1,  "upcoming:1")
assertTrue(3, 2,  "upcoming:1")
assertTrue(3, 3, "during:1")
assertTrue(3, 4, "during:1")
assertTrue(3, 5, "during:1")
assertTrue(3, 6, "during:1")


return
