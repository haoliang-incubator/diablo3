﻿
isArrayEmpty(mixed) {
    if (! isObject(mixed)) {
        return true
    }

    return mixed.Length() < 1
}

if (! isArrayEmpty("")) {
    MsgBox, "isArrayEmpty('') expect to true"
}

if (! isArrayEmpty([])) {
    MsgBox, "isArrayEmpty([]) expect to true"
}

if (isArrayEmpty(["a"])) {
    MsgBox, "isArrayEmpty(['a']) expect to false"
}

if (! isArrayEmpty(UNDECLARED_VAR)) {
    MsgBox, "isArrayEmpty(UNDECLARED_VAR) expect to true"
}
