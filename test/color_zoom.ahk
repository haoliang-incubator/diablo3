﻿#SingleInstance force

;Up::
;MouseGetPos, x, y
;y --
;MouseMove, %x%, %y%
;return
;
;Down::
;MouseGetPos, x, y
;y ++
;MouseMove, %x%, %y%
;return
;
;Left::
;MouseGetPos, x, y
;x --
;MouseMove, %x%, %y%
;return
;
;Right::
;MouseGetPos, x, y
;x ++
;MouseMove, %x%, %y%
;return

; Example: Achieve an effect similar to SplashTextOn:
;Gui, +AlwaysOnTop +Disabled -SysMenu +Owner  ; +Owner avoids a taskbar button.
;Gui, Add, Text,, Some text to display.
;Gui, Color, EEAA99
;Gui, Show, NoActivate, Title of Window  ; NoActivate avoids deactivating the currently active window.
;return

; Example: On-screen display (OSD) via transparent window:
CustomColor = 0xFFFF00
Gui +LastFound +AlwaysOnTop -Caption +ToolWindow  ; +ToolWindow avoids a taskbar button and an alt-tab menu item.
;Gui, Color, %CustomColor%
Gui, Margin, 0, 0
Gui, Font, s12  ; Set a large font size (32-point).
;XXXXXXXXX
Gui, Add, Text, vMyText cYellow, XXXXXXXX ******** ; XX & YY serve to auto-size the window.
; Make all pixels of this color transparent and make the text itself translucent (150):
WinSet, TransColor, %CustomColor% 150
;SetTimer, UpdateOSD, 200
;Gosub, UpdateOSD  ; Make the first update immediate rather than waiting for the timer.
Gui, Show, x835 y1305 NoActivate  ; NoActivate avoids deactivating the currently active window.
return

UpdateOSD:
MouseGetPos, MouseX, MouseY
GuiControl,, MyText, X%MouseX%, Y%MouseY%
return
