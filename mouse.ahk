﻿; author: haoliang<haoliang0.1.2@gmail.com>

; 仅在d3中生效
#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "mouse"
;}}}

#Include %A_ScriptDir%\lib\function\notification.ahk

#Include %A_ScriptDir%\lib\keybind\management.ahk
#Include %A_ScriptDir%\lib\keybind\click.ahk

w:: ;{{{
aKeyDown := !aKeyDown
If aKeyDown {
	Send {Click down left}
} Else {
	Send {Click up left}
}
return ;}}}

#IfWinActive
