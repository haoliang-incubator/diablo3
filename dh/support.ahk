﻿; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "dh support"
APP_STARTUP_LABELS := ["q", "e"]

; vengeance 750ms
; shadow power 4950ms
; companion 750ms
; preparation 2500ms
; smoke screen
; strafe 扫射

;}}}

; includes
;{{{
#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\toggle.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
#Include %A_ScriptDir%\..\lib\keybind\startup.ahk
#Include %A_ScriptDir%\..\lib\keybind\access.ahk
;}}}

; labels
; {{{

Companion:
SendInput {space down}{click right}{space up}
Return

EntanglingShot:
SendInput d
Return

Bolas:
SendInput a
Return

;}}}

; 缠绕射击
w::
wToggle := !wToggle
eToggle := False
if wToggle {
	SetTimer, EntanglingShot, off
	SetTimer, Bolas, 150
} else {
	SetTimer, EntanglingShot, off
	SetTimer, Bolas, off
}
Return

; 流星锁
e::
eToggle := !eToggle
wToggle := False
if eToggle {
	SetTimer, Bolas, off
	SetTimer, EntanglingShot, 150
} else {
	SetTimer, EntanglingShot, off
	SetTimer, Bolas, off
}
Return

#IfWinActive
