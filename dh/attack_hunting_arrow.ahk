﻿; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

; definitions
;{{{
APP_NAME := "dh attack - hunting arrow"
APP_STARTUP_LABELS := ["q", "e"]

; vengeance 750ms
; shadow power 4950ms
; companion 750ms
; preparation 2500ms
; smoke screen
; strafe 扫射

;}}}

; includes
;{{{
#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\cursor.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk
#Include %A_ScriptDir%\..\lib\function\toggle.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
#Include %A_ScriptDir%\..\lib\keybind\startup.ahk
#Include %A_ScriptDir%\..\lib\keybind\access.ahk
;}}}

; labels
; {{{

Preparation:
SendInput a
Return

ShadowPower:
SendInput s
Return

Vengeance:
SendInput d
Return

Companion:
SendInput, {click right}
Return

HuntingArrow:
SendInput {space down}{click left}{space up}
Return

KeepStrafing:
SendInput {f down}
return

StopStrafing:
SendInput {f up}
return

;}}}

; 隐式技能
e:: ;{{{
eToggle := !eToggle
If eToggle {
	SetTimer, Vengeance, 750

	SetTimer, Preparation, 1750
	SetTimer, ShadowPower, 1950
	SetTimer, Companion, 5000
	SetTimer, HuntingArrow, 1750

	Gosub, Preparation
	Gosub, ShadowPower
	Gosub, Companion
} Else {
	SetTimer, Vengeance, off
	SetTimer, Preparation, off
	SetTimer, Companion, off
	SetTimer, HuntingArrow, off
	SetTimer, ShadowPower, off
}
return
;}}}


XButton1::
strafeToggle := ! strafeToggle
if strafeToggle {
	SetTimer, KeepStrafing, 250
} else {
	SetTimer, KeepStrafing, off
	Gosub, StopStrafing
}
return

XButton2::
Goto, XButton1
return

z::
if startupToggle {
	gosub, F5
} else {
	ToggleOff(["q", "e"])
}

if strafeToggle {
	Gosub, XButton1
}
Gosub, Vengeance
sleep, % 2 * Ceil(1000/60)
SendInput, h
return

#IfWinActive
