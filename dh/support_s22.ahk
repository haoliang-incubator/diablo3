﻿
; author: haoliang<haoliang0.1.2@gmail.com>

#IfWinActive ahk_class D3 Main Window Class

APP_STARTUP_LABELS := ["e", "t"]


#Include %A_ScriptDir%\..\lib\meta\set.ahk

#Include %A_ScriptDir%\..\lib\function\notification.ahk
#Include %A_ScriptDir%\..\lib\function\toggle.ahk
#Include %A_ScriptDir%\..\lib\function\var.ahk

#Include %A_ScriptDir%\..\lib\keybind\management.ahk
#Include %A_ScriptDir%\..\lib\keybind\click.ahk
#Include %A_ScriptDir%\..\lib\keybind\access.ahk
#Include %A_ScriptDir%\..\lib\keybind\startup.ahk


TimedEntanglingShot:
SendInput {space down}{click left}{space up}
return

EntanglingShot:
SendInput {click left}
return

Multishot:
SendInput {click right}
return

Sentry:
SendInput {a}
return

Vault:
SendInput {s}
return

MarkedForDeath:
SendInput {d}
return

Companion:
SendInput {f}
return

$d::
SendInput {a}{d}
return

$Space::
Gosub, Multishot
SendInput {space down}
tToggleNow := tToggle
if tToggleNow {
    ToggleOff(["t"])
}
SetTimer, EntanglingShot, 300
SetTimer, Multishot, 2750
KeyWait, space
if tToggleNow {
    ToggleOn(["t"])
}
SetTimer, EntanglingShot, off
SetTimer, Multishot, off
SendInput {space up}
return


e::
eToggle := !eToggle
if eToggle {
    SetTimer, Companion, 500
} else {
    SetTimer, Companion, off
}
return

t::
tToggle := !tToggle
if tToggle {
    SetTimer, TimedEntanglingShot, 4750
    Gosub, TimedEntanglingShot
} else {
    SetTimer, TimedEntanglingShot, off
}
return


#IfWinActive
